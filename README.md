# Moolang

## What is this?

This project is a compiler that I am building in order to learn more about how
compilers and progamming languages work. I'm basing the compiler off of what I
learned from "[Writing an Interpreter in Go](https://interpreterbook.com/)" by
[Thorsten Ball](https://twitter.com/thorstenball) and
"[Crafting Interpreters](https://craftinginterpreters.com/)" by
[Robert Nystrom](https://twitter.com/munificentbob).

I realize that both of these books are called something...something Interpreters
something...something and I said this was a compiler but I have ambitions.
¯\\_(ツ)_/¯ Actually, the second half of "Crafting Interpreters" and the follow-up
book to "Writing an Interpreter in Go" cover compiling to bytecode to run on a
virtual machine but I want to use LLVM to make native binaries.

## What is Moolang?

Moo is a programming language that I am creating based on combinaton of other
languages that I really like with some new ideas thrown in to keep things
interesting. The current state of the context-free grammar is in
[grammar.md](grammar.md) and is kept up to date with development. I've done some
planning around the syntax and this can be seen in the [Moo Book repo](https://bitbucket.org/cborders/moolang-book).

## How to Moo?

### Install Rust
The Moo compiler is written in [Rust](https://www.rust-lang.org) so the first
step is to make sure that you have Rust installed by following their
[installation guide](https://www.rust-lang.org/tools/install).

### Build and run Moo
Once Rust is installed all you have to do to run Moo is run `cargo run`. This will
start the Moo compiler in REPL mode and allow you to play with Moo in the
command line.

If you want to compile files that you've written just pass them as arguments to
the cargo command.

```
cargo run -- myFile.moo
```

To find more information about running the Moo compiler use the `--help` command.

```
cargo run -- --help
```

### Building in release mode
If you want to build the Moo compiler in release mode you can add `--release` to
the build command.

```
cargo build --release
```

This will output an optimized application binary in the following directory:

```
<moo_root>/target/release
```

If you add that directory to your system path you can invoke the Moo compiler
from anywhere on your system.
