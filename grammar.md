```
declaration → varDecl | statement ;
varDecl     → ( "var" | "let" ) IDENTIFIER ( note_type | assign_value | note_type assign_value ) EOL ;
```

```
statement → exprStmt | scopeStmt | whenStmt | printStmt | block ;
exprStmt  → expression EOL ;
scopeStmt → "@" ( "global" | "file" | IDENTIFIER ) ;
whenStmt  → "when" 
            | expression block ( "else" block )? 
            | ( "(" expression ")" )? "{" (expression ":" statememt)* ( "else" statement )? "}" ;
whileStmt → "while" expression block ;
printStmt → "print" expression EOL ;
block     → "{" declaration* "}" ;
```

```
expression → assignment ;
assignment → IDENTIFIER "=" assignment | logic_or ;
logic_or   → logic_and ( "|" logic_and )* ;
logic_and  → equality ( "&" equality )* ;
equality   → comparison ( ( "!=" | "==" ) comparison )* ;
comparison → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
term       → factor ( ( "-" | "+" ) factor )* ;
factor     → unary ( ( "/" | "*" ) unary )* ;
unary      → ( "!" | "-" ) unary | cast ;
cast       → primary ( "as" TYPE )? ;
primary    → NUMBER | STRING | IDENTIFIER | "true" | "false" | "(" expression ")" ;
```

```
assign_value → "=" assignment ;
note_type    → ":" TYPE ;
```

```
NUMBER     → DIGIT ( DIGIT | "_" )* ( "." DIGIT ( DIGIT | "_" )* )? ;
STRING     → "\"" <any char except "\"">* "\"" ;
IDENTIFIER → ALPHA ( ALPHA | DIGIT )* ;
ALPHA      → "a" ... "z" | "A" ... "Z" | "_" ;
DIGIT      → "0" ... "9" ;
```