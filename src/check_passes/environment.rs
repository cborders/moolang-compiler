use {
    crate::{
        lexer::{Location, Token},
        types::MooType,
    },
    anyhow::{anyhow, Error},
    std::{cell::RefCell, collections::HashMap, rc::Rc},
};

#[derive(Clone, Debug)]
pub struct VarInfo {
    pub decl_loc: Location,
    pub constant: bool,
    pub moo_type: MooType,
}

#[derive(Debug)]
pub struct Scope {
    values: RefCell<HashMap<String, VarInfo>>,
    parent: RefCell<Option<Rc<Scope>>>,
}

impl Scope {
    pub fn new() -> Self {
        Self {
            values: RefCell::new(HashMap::new()),
            parent: RefCell::new(None),
        }
    }

    pub fn push_new(self: &Rc<Self>) -> Self {
        Self {
            values: RefCell::new(HashMap::new()),
            parent: RefCell::new(Some(Rc::clone(self))),
        }
    }

    pub fn reparent(&self, parent: &Rc<Self>) {
        *self.parent.borrow_mut() = Some(Rc::clone(parent))
    }

    pub fn define(&self, ident: &Token, constant: &bool, moo_type: MooType) -> Result<(), Error> {
        if let Token::Ident { loc, lexeme } = ident {
            if self.values.borrow().get(lexeme).is_some() {
                let info = self.values.borrow().get(lexeme).unwrap().clone();
                Err(anyhow!("A variable named '{}' already exists in this scope. Previous Definition at: {}", lexeme, info.decl_loc))
            } else {
                let info = VarInfo {
                    decl_loc: loc.clone(),
                    constant: *constant,
                    moo_type,
                };
                self.values.borrow_mut().insert(lexeme.to_owned(), info);
                Ok(())
            }
        } else {
            Err(anyhow!("Expected identifier but found {:?}", ident))
        }
    }

    pub fn get(&self, ident: &String) -> Result<MooType, Error> {
        if !self.values.borrow().contains_key(ident) {
            if let Some(parent) = &*self.parent.borrow() {
                parent.get(ident)
            } else {
                Err(anyhow!("'{}' is not defined", ident))
            }
        } else {
            let info = self.values.borrow().get(ident).unwrap().clone();
            Ok(info.moo_type.clone())
        }
    }

    pub fn get_info(&self, ident: &String) -> Option<VarInfo> {
        if !self.values.borrow().contains_key(ident) {
            if let Some(parent) = &*self.parent.borrow() {
                parent.get_info(ident)
            } else {
                None
            }
        } else {
            Some(self.values.borrow().get(ident).unwrap().clone())
        }
    }
}

pub struct Environment {
    pub(crate) global_scope: Rc<Scope>,
    file_scopes: RefCell<HashMap<String, Rc<Scope>>>,
    named_scopes: RefCell<HashMap<String, Rc<Scope>>>,
}

impl Environment {
    pub fn new() -> Self {
        Environment {
            global_scope: Rc::new(Scope::new()),
            file_scopes: RefCell::new(HashMap::new()),
            named_scopes: RefCell::new(HashMap::new()),
        }
    }

    pub fn file_scope(&self, path: &str) -> Rc<Scope> {
        if self.file_scopes.borrow().contains_key(path) {
            Rc::clone(self.file_scopes.borrow().get(path).unwrap())
        } else {
            let scope = Rc::new(Scope::new());
            self.file_scopes
                .borrow_mut()
                .insert(path.to_owned(), Rc::clone(&scope));
            scope
        }
    }

    pub fn named_scope(&self, name: &str) -> Rc<Scope> {
        if self.named_scopes.borrow().contains_key(name) {
            Rc::clone(self.named_scopes.borrow().get(name).unwrap())
        } else {
            let scope = Rc::new(Scope::new());
            self.named_scopes
                .borrow_mut()
                .insert(name.to_owned(), Rc::clone(&scope));
            scope
        }
    }
}
