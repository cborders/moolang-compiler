use {
    crate::{errors::format_error, lexer::Token, parser::Expr, types::MooType},
    anyhow::Error,
};

pub trait Evaluator {
    fn evaluate_assignment(&mut self, ident: &Token, moo_type: MooType) -> Result<MooType, Error>;
    fn evaluate_variable(&mut self, ident: &Token) -> Result<MooType, Error>;

    fn evaluate(&mut self, expr: &Expr) -> Result<MooType, Error> {
        match expr {
            Expr::Assignment { ident, expr } => {
                let moo_type = self.evaluate(expr)?;
                self.evaluate_assignment(ident, moo_type)
            }
            Expr::BoolLiteral { value: _ } => Ok(MooType::Bool),
            Expr::Binary { left, oper, right } => {
                let left_moo_type = self.evaluate(left)?;
                let right_moo_type = self.evaluate(right)?;
                self.evaluate_binary(left_moo_type, oper, right_moo_type)
            }
            Expr::Cast { expr, cast_type } => {
                let moo_type = self.evaluate(expr)?;
                self.evaluate_cast(moo_type, cast_type)
            }
            Expr::Grouping { expr } => self.evaluate(expr),
            Expr::StringLiteral { value: _ } => Ok(MooType::String),
            Expr::NumberLiteral { value: token } => self.evaluate_number(token),
            Expr::Unary { oper, expr } => {
                let moo_type = self.evaluate(expr)?;
                self.evaluate_unary(oper, moo_type)
            }
            Expr::Var { ident } => self.evaluate_variable(ident),
        }
    }

    fn evaluate_bool(&mut self, token: &Token) -> Result<MooType, Error> {
        match token {
            Token::True { loc: _ } => Ok(MooType::Bool),
            Token::False { loc: _ } => Ok(MooType::Bool),
            _ => Err(format_error(token, "Expected bool literal")),
        }
    }

    fn evaluate_binary(
        &mut self,
        left: MooType,
        oper: &Token,
        right: MooType,
    ) -> Result<MooType, Error> {
        if left.is_numeric() && right.is_numeric() {
            self.evaluate_numeric_binary(left, oper, right)
        } else if left != right {
            Err(format_error(
                oper,
                &format!(
                    "Binary operator is invalid for left: {:?}, right: {:?}",
                    left, right
                ),
            ))
        } else if left == MooType::String {
            match oper {
                Token::Plus { loc: _ } => Ok(MooType::String),
                Token::EqualEqual { loc: _ } => Ok(MooType::Bool),
                Token::BangEqual { loc: _ } => Ok(MooType::Bool),
                _ => Err(format_error(
                    oper,
                    "Only +, ==, and != are a valid binary operators for strings",
                )),
            }
        } else if left == MooType::Bool {
            match oper {
                Token::Ampersand { loc: _ } => Ok(MooType::Bool),
                Token::Pipe { loc: _ } => Ok(MooType::Bool),
                Token::EqualEqual { loc: _ } => Ok(MooType::Bool),
                Token::BangEqual { loc: _ } => Ok(MooType::Bool),
                _ => Err(format_error(
                    oper,
                    "Only &, |, ==, and != are valid binary operators for bool values",
                )),
            }
        } else {
            Err(format_error(
                oper,
                "Only string and numerical types are supported in binary operations",
            ))
        }
    }

    fn evaluate_numeric_binary(
        &mut self,
        left: MooType,
        oper: &Token,
        right: MooType,
    ) -> Result<MooType, Error> {
        if (*oper).is_arithmetic() {
            MooType::result_type(&left, &right)
        } else if (*oper).is_comparison() {
            if left == right {
                Ok(MooType::Bool)
            } else {
                Err(format_error(
                    oper,
                    &format!(
                        "{} is not a valid operator for left: {:?} right: {:?}",
                        oper, left, right
                    ),
                ))
            }
        } else {
            Err(format_error(
                oper,
                "Only arithmetic and comparison operators are valid between numeric types",
            ))
        }
    }

    fn evaluate_cast(&mut self, moo_type: MooType, cast: &Token) -> Result<MooType, Error> {
        let cast_type = MooType::from_token(cast)?;
        if moo_type.is_numeric() && cast_type.is_numeric() {
            Ok(cast_type)
        } else if cast_type == MooType::String {
            Ok(cast_type)
        } else {
            Err(format_error(
                cast,
                &format!("Cannot cast from {} to {}", moo_type, cast_type),
            ))
        }
    }

    fn evaluate_unary(&mut self, oper: &Token, moo_type: MooType) -> Result<MooType, Error> {
        if moo_type.is_numeric() {
            if let Token::Minus { loc: _ } = oper {
                Ok(moo_type)
            } else {
                Err(format_error(
                    oper,
                    &format!("Only - is supported as a unary operator with numerical types"),
                ))
            }
        } else if moo_type == MooType::Bool {
            if let Token::Bang { loc: _ } = oper {
                Ok(moo_type)
            } else {
                Err(format_error(
                    oper,
                    &format!("Only ! is supported as a unary operator with bool types"),
                ))
            }
        } else {
            Err(format_error(
                oper,
                &format!("Unary operators are only valid with numerical or bool types"),
            ))
        }
    }

    fn evaluate_string(&mut self, token: &Token) -> Result<MooType, Error> {
        match token {
            Token::StringLiteral { loc: _, lexeme: _ } => Ok(MooType::String),
            _ => Err(format_error(token, "Expected string literal")),
        }
    }

    fn evaluate_number(&mut self, token: &Token) -> Result<MooType, Error> {
        match token {
            Token::Number { loc: _, lexeme } => {
                if lexeme.contains(".") {
                    Ok(MooType::Float32)
                } else {
                    Ok(MooType::Int32)
                }
            }
            _ => Err(format_error(token, "Expected number literal")),
        }
    }
}
