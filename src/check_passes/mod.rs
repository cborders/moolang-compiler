mod definer;
mod environment;
mod evaluator;
mod type_checker;

use {crate::program::Program, anyhow::Error, environment::Environment};

pub fn check(program: &Program) -> Result<(), Vec<Error>> {
    let mut environment = Environment::new();
    definer::define(program, &mut environment)?;
    type_checker::type_check(program, &mut environment)?;
    Ok(())
}
