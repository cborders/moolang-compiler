use {
    super::{
        environment::{Environment, Scope},
        evaluator::Evaluator,
    },
    crate::{
        errors::format_error,
        lexer::Token,
        parser::{ConditionalStatement, Expr, Stmt},
        program::{File, Program},
        types::MooType,
    },
    anyhow::{anyhow, Error},
    if_chain::if_chain,
    std::rc::Rc,
};

#[derive(PartialEq)]
enum ScopeType {
    Global,
    File,
    Named,
}

pub fn type_check(program: &Program, environment: &mut Environment) -> Result<(), Vec<Error>> {
    let mut type_checker = TypeChecker::new(program, environment);
    type_checker.check()
}

struct TypeChecker<'a> {
    program: &'a Program,
    env: &'a Environment,
    curr_file: &'a File,
    curr_scope: Rc<Scope>,
    scope_type: ScopeType,
}

impl<'a> TypeChecker<'a> {
    pub fn new(program: &'a Program, environment: &'a Environment) -> Self {
        let curr_file = &program.files[0];
        let file_scope = environment.file_scope(&curr_file.path);

        Self {
            program,
            env: environment,
            curr_file,
            curr_scope: file_scope,
            scope_type: ScopeType::File,
        }
    }

    pub fn check(&mut self) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for file in &self.program.files {
            for stmt in &file.stmts {
                if let Err(mut stmt_errors) = self.check_stmt(&stmt) {
                    errors.append(&mut stmt_errors);
                }
            }
        }

        if errors.len() > 0 {
            Err(errors)
        } else {
            Ok(())
        }
    }

    fn check_stmt(&mut self, stmt: &Stmt) -> Result<(), Vec<Error>> {
        match stmt {
            Stmt::Block { stmts } => self.block(stmts),
            Stmt::Expr { expr } => self.expression_statement(expr),
            Stmt::Print { expr } => {
                if let Err(error) = self.evaluate(expr) {
                    Err(vec![error])
                } else {
                    Ok(())
                }
            }
            Stmt::Scope { token } => self.scope_statement(token),
            Stmt::VarDecl {
                ident,
                constant,
                var_type,
                expr,
            } => self.var_declaration(ident, constant, var_type, expr),
            Stmt::When {
                conditions,
                else_stmt,
            } => self.when_statement(conditions, else_stmt),
            Stmt::While { condition, block } => self.while_statement(condition, block),
        }
    }

    fn block(&mut self, stmts: &Vec<Box<Stmt>>) -> Result<(), Vec<Error>> {
        let old_scope = Rc::clone(&self.curr_scope);
        self.curr_scope = Rc::new(self.curr_scope.push_new());

        let mut errors = vec![];
        for stmt in stmts {
            if let Err(mut stmt_errors) = self.check_stmt(stmt) {
                errors.append(&mut stmt_errors);
            }
        }

        self.curr_scope = old_scope;

        if errors.len() > 0 {
            Err(errors)
        } else {
            Ok(())
        }
    }

    fn expression_statement(&mut self, expr: &Expr) -> Result<(), Vec<Error>> {
        if let Err(error) = self.evaluate(expr) {
            Err(vec![error])
        } else {
            Ok(())
        }
    }

    fn scope_statement(&mut self, token: &Token) -> Result<(), Vec<Error>> {
        match token {
            Token::GlobalScope { loc: _ } => {
                self.curr_scope = Rc::clone(&self.env.global_scope);
                self.scope_type = ScopeType::Global;
            }
            Token::FileScope { loc: _ } => {
                self.curr_scope = self.env.file_scope(&self.curr_file.path);
                self.scope_type = ScopeType::File;
            }
            Token::NamedScope { loc: _, lexeme } => {
                self.scope_type = ScopeType::Named;
                let named_scope = self.env.named_scope(lexeme);
                named_scope.reparent(&self.env.file_scope(&self.curr_file.path));
                self.curr_scope = named_scope;
            }
            _ => return Err(vec![anyhow!("Expected a scope, got {}", token)]),
        }
        Ok(())
    }

    fn var_declaration(
        &mut self,
        ident: &Token,
        constant: &bool,
        var_type: &Option<Token>,
        expr: &Option<Box<Expr>>,
    ) -> Result<(), Vec<Error>> {
        if let Token::Ident { loc, lexeme } = ident {
            if var_type.is_none() && expr.is_none() {
                return Err(vec![format_error(
                    ident,
                    "A variable must have a type known at compile time",
                )]);
            }

            let mut errors = vec![];

            let var_type = if let Some(var_type) = var_type {
                match MooType::from_token(var_type) {
                    Ok(moo_type) => Some(moo_type),
                    Err(error) => {
                        errors.push(error);
                        None
                    }
                }
            } else {
                None
            };

            let expr_type = if let Some(expr) = expr {
                match self.evaluate(expr) {
                    Ok(moo_type) => Some(moo_type),
                    Err(error) => {
                        errors.push(error);
                        None
                    }
                }
            } else {
                None
            };

            if_chain! {
                if let Some(ref var_type) = var_type;
                if let Some(ref expr_type) = expr_type;
                if var_type != expr_type;
                then {
                    errors.push(format_error(ident, &format!("Cannot assign {} to a variable of type {}", expr_type, var_type)));
                    return Err(errors);
                }
            }

            let moo_type = match var_type {
                Some(moo_type) => moo_type,
                None => expr_type.unwrap(),
            };

            if let Err(error) = self.curr_scope.define(ident, constant, moo_type) {
                // If we are in global or a named scope this variable should have
                // already been defined in a previous pass so let's just verify
                // that it's the same decl and has the right type.
                if_chain! {
                    if self.scope_type == ScopeType::Global || self.scope_type == ScopeType::Named;
                    if let Some(info) = self.curr_scope.get_info(lexeme);
                    if info.decl_loc == *loc;
                    then {
                        // This is the same variable that's already defined so
                        // check that it has the right type
                        if info.moo_type == moo_type {
                            Ok(())
                        } else {
                            Err(vec![format_error(ident, "Got the wrong type in the define pass. This is a really bad message.")])
                        }
                    } else {
                        Err(vec![format_error(ident, &format!("{:?}", error))])
                    }
                }
            } else {
                Ok(())
            }
        } else {
            Err(vec![format_error(ident, "Expected identifier")])
        }
    }

    fn when_statement(
        &mut self,
        conditions: &Vec<ConditionalStatement>,
        else_stmt: &Option<Box<Stmt>>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for condition in conditions {
            match self.evaluate(&condition.condition) {
                Ok(condition_type) => {
                    if condition_type != MooType::Bool {
                        errors.push(format_error(
                            condition.condition.token(),
                            "Conditions but result in a bool value",
                        ));
                    }

                    if let Err(mut stmt_errors) = self.check_stmt(&condition.statement) {
                        errors.append(&mut stmt_errors);
                    }
                }
                Err(error) => errors.push(error),
            }
        }

        if let Some(else_stmt) = else_stmt {
            if let Err(mut stmt_errors) = self.check_stmt(else_stmt) {
                errors.append(&mut stmt_errors);
            }
        }

        if errors.len() > 0 {
            Err(errors)
        } else {
            Ok(())
        }
    }

    fn while_statement(
        &mut self,
        condition: &Box<Expr>,
        block: &Box<Stmt>,
    ) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        match self.evaluate(condition) {
            Ok(condition_type) => {
                if condition_type != MooType::Bool {
                    errors.push(format_error(
                        condition.token(),
                        "Conditions but result in a bool value",
                    ));
                }
            }
            Err(error) => errors.push(error),
        }

        if let Err(mut stmt_errors) = self.check_stmt(block) {
            errors.append(&mut stmt_errors);
        }

        if errors.len() > 0 {
            Err(errors)
        } else {
            Ok(())
        }
    }
}

impl<'a> Evaluator for TypeChecker<'a> {
    fn evaluate_assignment(&mut self, ident: &Token, moo_type: MooType) -> Result<MooType, Error> {
        if let Token::Ident { loc: _, lexeme } = ident {
            let info = match self.curr_scope.get_info(lexeme) {
                Some(info) => info,
                None => return Err(format_error(ident, &format!("{} is not defined", lexeme))),
            };

            if info.constant {
                Err(format_error(ident, "Cannot assign to a constant value"))
            } else if info.moo_type != moo_type {
                Err(format_error(
                    ident,
                    &format!(
                        "Cannot assign {} to a variable of type {}",
                        moo_type, info.moo_type
                    ),
                ))
            } else {
                Ok(info.moo_type)
            }
        } else {
            Err(format_error(ident, "Expected identifier"))
        }
    }

    fn evaluate_variable(&mut self, ident: &Token) -> Result<MooType, Error> {
        if let Token::Ident { loc: _, lexeme } = ident {
            self.curr_scope.get(lexeme)
        } else {
            Err(format_error(ident, "Expected identifier"))
        }
    }
}
