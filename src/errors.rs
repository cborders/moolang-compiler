use {
    crate::lexer::Token,
    anyhow::{anyhow, Error},
};

pub fn format_error(token: &Token, message: &str) -> Error {
    anyhow!("{} {}", token.location(), message)
}
