use {
    super::{
        environment::{Environment, Scope},
        evaluator::Evaluator,
        value::Value,
    },
    crate::{
        errors::format_error,
        lexer::Token,
        parser::{Expr, Stmt},
        program::{File, Program},
    },
    anyhow::{anyhow, Error},
    if_chain::if_chain,
    std::rc::Rc,
};

#[derive(PartialEq)]
enum ScopeType {
    Global,
    File,
    Named,
}

pub fn define(program: &Program, environment: &mut Environment) -> Result<(), Vec<Error>> {
    let mut definer = Definer::new(program, environment);
    definer.define()
}

struct Definer<'a> {
    program: &'a Program,
    env: &'a Environment,
    curr_file: &'a File,
    curr_scope: Rc<Scope>,
    scope_type: ScopeType,
}

impl<'a> Definer<'a> {
    pub fn new(program: &'a Program, environment: &'a Environment) -> Self {
        let curr_file = &program.files[0];
        let file_scope = environment.file_scope(&curr_file.path);

        Self {
            program,
            env: environment,
            curr_file,
            curr_scope: file_scope,
            scope_type: ScopeType::File,
        }
    }

    pub fn define(&mut self) -> Result<(), Vec<Error>> {
        let mut errors = vec![];
        for file in &self.program.files {
            for stmt in &file.stmts {
                if let Err(error) = self.define_stmt(&stmt) {
                    errors.push(error);
                }
            }
        }

        if errors.len() > 0 {
            Err(errors)
        } else {
            Ok(())
        }
    }

    fn define_stmt(&mut self, stmt: &Stmt) -> Result<(), Error> {
        match stmt {
            Stmt::Expr { expr } => self.expression_statement(expr),
            Stmt::Scope { token } => self.scope_statement(token),
            Stmt::VarDecl {
                ident,
                constant,
                var_type,
                expr,
            } => self.var_declaration(ident, constant, var_type, expr),
            _ => Ok(()),
        }
    }

    fn expression_statement(&mut self, _expr: &Expr) -> Result<(), Error> {
        // This is going to come into play later as we have nested scopes.
        Ok(())
    }

    fn scope_statement(&mut self, token: &Token) -> Result<(), Error> {
        match token {
            Token::GlobalScope { loc: _ } => {
                self.curr_scope = Rc::clone(&self.env.global_scope);
                self.scope_type = ScopeType::Global;
            }
            Token::FileScope { loc: _ } => {
                self.curr_scope = self.env.file_scope(&self.curr_file.path);
                self.scope_type = ScopeType::File;
            }
            Token::NamedScope { loc: _, lexeme } => {
                self.scope_type = ScopeType::Named;
                let named_scope = self.env.named_scope(lexeme);
                named_scope.reparent(&self.env.file_scope(&self.curr_file.path));
                self.curr_scope = named_scope;
            }
            _ => return Err(anyhow!("Expected a scope, got {}", token)),
        }
        Ok(())
    }

    fn var_declaration(
        &mut self,
        ident: &Token,
        constant: &bool,
        var_type: &Option<Token>,
        expr: &Option<Box<Expr>>,
    ) -> Result<(), Error> {
        if self.scope_type == ScopeType::Global || self.scope_type == ScopeType::Named {
            if let Token::Ident { loc, lexeme } = ident {
                // If we are in global or a named scope this variable could have
                // already been defined in a previous pass so let's just verify
                // that it's the same decl.
                if_chain! {
                    if let Some(info) = self.curr_scope.get_info(lexeme);
                    if info.decl_loc == *loc;
                    then {
                        // This is the same variable that's already defined
                        // so we can just stop here
                        return Ok(());
                    }
                }

                // TODO(Casey): Use the var_type to make sure this is correct.
                let value = if let Some(expr) = expr {
                    Some(self.evaluate(expr)?)
                } else {
                    None
                };

                if let Err(error) = self.curr_scope.define(ident, constant, value) {
                    Err(format_error(ident, &format!("{:?}", error)))
                } else {
                    Ok(())
                }
            } else {
                Err(format_error(ident, "Expected identifier"))
            }
        } else {
            Ok(())
        }
    }
}

impl<'a> Evaluator for Definer<'a> {
    fn evaluate_assignment(&mut self, _ident: &Token, _value: Value) -> Result<Value, Error> {
        Err(anyhow!("Don't do assignments here."))
    }

    fn evaluate_variable(&mut self, ident: &Token) -> Result<Value, Error> {
        if_chain! {
            if self.scope_type == ScopeType::Global || self.scope_type == ScopeType::Named;
            if let Token::Ident { loc: _, lexeme } = ident;
            then {
                self.curr_scope.get(lexeme)
            } else {
                Err(format_error(ident, "Expected identifier"))
            }
        }
    }
}
