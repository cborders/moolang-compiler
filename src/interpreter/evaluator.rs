use {
    super::value::Value,
    crate::{errors::format_error, lexer::Token, parser::Expr, types::MooType},
    anyhow::Error,
    if_chain::if_chain,
    num_traits::Num,
    std::rc::Rc,
};

pub trait Evaluator {
    fn evaluate_assignment(&mut self, ident: &Token, value: Value) -> Result<Value, Error>;
    fn evaluate_variable(&mut self, ident: &Token) -> Result<Value, Error>;

    fn evaluate(&mut self, expr: &Expr) -> Result<Value, Error> {
        match expr {
            Expr::Assignment { ident, expr } => {
                let value = self.evaluate(expr)?;
                self.evaluate_assignment(ident, value)
            }
            Expr::BoolLiteral { value } => self.evaluate_bool(value),
            Expr::Binary { left, oper, right } => {
                let left_value = self.evaluate(left)?;
                let right_value = self.evaluate(right)?;
                self.evaluate_binary(left_value, oper, right_value)
            }
            Expr::Cast { expr, cast_type } => {
                let value = self.evaluate(expr)?;
                self.evaluate_cast(value, cast_type)
            }
            Expr::Grouping { expr } => self.evaluate(expr),
            Expr::StringLiteral { value } => self.evaluate_string(value),
            Expr::NumberLiteral { value } => self.evaluate_number(value),
            Expr::Unary { oper, expr } => {
                let value = self.evaluate(expr)?;
                self.evaluate_unary(oper, value)
            }
            Expr::Var { ident } => self.evaluate_variable(ident),
        }
    }

    fn evaluate_bool(&mut self, value: &Token) -> Result<Value, Error> {
        match value {
            Token::True { loc: _ } => Ok(Value {
                inner: Rc::new(true),
                moo_type: MooType::Bool,
            }),
            Token::False { loc: _ } => Ok(Value {
                inner: Rc::new(false),
                moo_type: MooType::Bool,
            }),
            _ => Err(format_error(value, "Expected bool literal")),
        }
    }

    fn evaluate_binary(&mut self, left: Value, oper: &Token, right: Value) -> Result<Value, Error> {
        if left.moo_type.is_numeric() && right.moo_type.is_numeric() {
            self.evaluate_numeric_binary(left, oper, right)
        } else if left.moo_type != right.moo_type {
            Err(format_error(
                oper,
                &format!(
                    "Binary operator is invalid for left: {:?}, right: {:?}",
                    left.moo_type, right.moo_type
                ),
            ))
        } else if left.moo_type == MooType::String {
            let left_str = left.as_type::<String>()?;
            let right_str = right.as_type::<String>()?;
            match oper {
                Token::Plus { loc: _ } => Ok(Value {
                    inner: Rc::new(left_str.to_owned() + &right_str),
                    moo_type: MooType::String,
                }),
                Token::EqualEqual { loc: _ } => self.equality_binary(left_str, oper, right_str),
                Token::BangEqual { loc: _ } => self.equality_binary(left_str, oper, right_str),
                _ => Err(format_error(
                    oper,
                    "Only + is a valid binary operator for strings",
                )),
            }
        } else if left.moo_type == MooType::Bool {
            let left_bool = left.as_type::<bool>()?;
            let right_bool = right.as_type::<bool>()?;
            match oper {
                Token::Ampersand { loc: _ } => self.boolean_binary(left_bool, oper, right_bool),
                Token::Pipe { loc: _ } => self.boolean_binary(left_bool, oper, right_bool),
                Token::EqualEqual { loc: _ } => self.equality_binary(left_bool, oper, right_bool),
                Token::BangEqual { loc: _ } => self.equality_binary(left_bool, oper, right_bool),
                _ => Err(format_error(
                    oper,
                    "Only &, |, ==, and != are valid binary operators for bool values",
                )),
            }
        } else {
            Err(format_error(
                oper,
                "Only string and numerical types are supported in binary operations",
            ))
        }
    }

    fn evaluate_numeric_binary(
        &mut self,
        left: Value,
        oper: &Token,
        right: Value,
    ) -> Result<Value, Error> {
        if (*oper).is_arithmetic() {
            let result_type = MooType::result_type(&left.moo_type, &right.moo_type)?;
            match result_type {
                MooType::Int8 => {
                    let left_num = left.as_type::<i8>()?;
                    let right_num = right.as_type::<i8>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Int16 => {
                    let left_num = left.as_type::<i16>()?;
                    let right_num = right.as_type::<i16>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Int32 => {
                    let left_num = left.as_type::<i32>()?;
                    let right_num = right.as_type::<i32>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Int64 => {
                    let left_num = left.as_type::<i64>()?;
                    let right_num = right.as_type::<i64>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Uint8 => {
                    let left_num = left.as_type::<u8>()?;
                    let right_num = right.as_type::<u8>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Uint16 => {
                    let left_num = left.as_type::<u16>()?;
                    let right_num = right.as_type::<u16>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Uint32 => {
                    let left_num = left.as_type::<u32>()?;
                    let right_num = right.as_type::<u32>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Uint64 => {
                    let left_num = left.as_type::<u64>()?;
                    let right_num = right.as_type::<u64>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Float32 => {
                    let left_num = left.as_type::<f32>()?;
                    let right_num = right.as_type::<f32>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                MooType::Float64 => {
                    let left_num = left.as_type::<f64>()?;
                    let right_num = right.as_type::<f64>()?;
                    let result = self.arithmetic_binary(left_num, oper, right_num)?;
                    Ok(Value {
                        inner: Rc::new(result),
                        moo_type: result_type,
                    })
                }
                _ => {
                    return Err(format_error(
                        oper,
                        &format!("Left is not a numeric type: {:?}", left.moo_type),
                    ))
                }
            }
        } else if (*oper).is_comparison() {
            if left.moo_type == right.moo_type {
                match left.moo_type {
                    MooType::Int8 => {
                        let left_num = left.as_type::<i8>()?;
                        let right_num = right.as_type::<i8>()?;
                        self.comparison_binary::<i8>(left_num, oper, right_num)
                    }
                    MooType::Int16 => {
                        let left_num = left.as_type::<i16>()?;
                        let right_num = right.as_type::<i16>()?;
                        self.comparison_binary::<i16>(left_num, oper, right_num)
                    }
                    MooType::Int32 => {
                        let left_num = left.as_type::<i32>()?;
                        let right_num = right.as_type::<i32>()?;
                        self.comparison_binary::<i32>(left_num, oper, right_num)
                    }
                    MooType::Int64 => {
                        let left_num = left.as_type::<i64>()?;
                        let right_num = right.as_type::<i64>()?;
                        self.comparison_binary::<i64>(left_num, oper, right_num)
                    }
                    MooType::Uint8 => {
                        let left_num = left.as_type::<u8>()?;
                        let right_num = right.as_type::<u8>()?;
                        self.comparison_binary::<u8>(left_num, oper, right_num)
                    }
                    MooType::Uint16 => {
                        let left_num = left.as_type::<u16>()?;
                        let right_num = right.as_type::<u16>()?;
                        self.comparison_binary::<u16>(left_num, oper, right_num)
                    }
                    MooType::Uint32 => {
                        let left_num = left.as_type::<u32>()?;
                        let right_num = right.as_type::<u32>()?;
                        self.comparison_binary::<u32>(left_num, oper, right_num)
                    }
                    MooType::Uint64 => {
                        let left_num = left.as_type::<u64>()?;
                        let right_num = right.as_type::<u64>()?;
                        self.comparison_binary::<u64>(left_num, oper, right_num)
                    }
                    MooType::Float32 => {
                        let left_num = left.as_type::<f32>()?;
                        let right_num = right.as_type::<f32>()?;
                        self.comparison_binary::<f32>(left_num, oper, right_num)
                    }
                    MooType::Float64 => {
                        let left_num = left.as_type::<f64>()?;
                        let right_num = right.as_type::<f64>()?;
                        self.comparison_binary::<f64>(left_num, oper, right_num)
                    }
                    _ => {
                        return Err(format_error(
                            oper,
                            &format!("Left is not a numeric type: {:?}", left.moo_type),
                        ))
                    }
                }
            } else {
                Err(format_error(
                    oper,
                    &format!(
                        "{} is not a valid operator for left: {:?} right: {:?}",
                        oper, left.moo_type, right.moo_type
                    ),
                ))
            }
        } else {
            Err(format_error(
                oper,
                "Only arithmetic and comparison operators are valid between numeric types",
            ))
        }
    }

    fn arithmetic_binary<T: Num>(&mut self, left: T, oper: &Token, right: T) -> Result<T, Error> {
        match oper {
            Token::Plus { loc: _ } => Ok(left + right),
            Token::Minus { loc: _ } => Ok(left - right),
            Token::Star { loc: _ } => Ok(left * right),
            Token::Slash { loc: _ } => Ok(left / right),
            Token::Percent { loc: _ } => Ok(left % right),
            _ => Err(format_error(
                oper,
                &format!("Invalid numeric operator: {}", oper),
            )),
        }
    }

    fn equality_binary<T: std::cmp::PartialEq>(
        &mut self,
        left: T,
        oper: &Token,
        right: T,
    ) -> Result<Value, Error> {
        let result = match oper {
            Token::EqualEqual { loc: _ } => left == right,
            Token::BangEqual { loc: _ } => left != right,
            _ => {
                return Err(format_error(
                    oper,
                    &format!("Invalid equality operator: {}", oper),
                ))
            }
        };

        Ok(Value {
            inner: Rc::new(result),
            moo_type: MooType::Bool,
        })
    }

    fn boolean_binary(&mut self, left: bool, oper: &Token, right: bool) -> Result<Value, Error> {
        let result = match oper {
            Token::Ampersand { loc: _ } => left && right,
            Token::Pipe { loc: _ } => left || right,
            _ => {
                return Err(format_error(
                    oper,
                    &format!("Invalid boolean operator: {}", oper),
                ))
            }
        };

        Ok(Value {
            inner: Rc::new(result),
            moo_type: MooType::Bool,
        })
    }

    fn comparison_binary<T: std::cmp::PartialOrd>(
        &mut self,
        left: T,
        oper: &Token,
        right: T,
    ) -> Result<Value, Error> {
        let result = match oper {
            Token::Greater { loc: _ } => left > right,
            Token::Less { loc: _ } => left < right,
            Token::GreaterEqual { loc: _ } => left >= right,
            Token::LessEqual { loc: _ } => left <= right,
            Token::EqualEqual { loc: _ } => return self.equality_binary(left, oper, right),
            Token::BangEqual { loc: _ } => return self.equality_binary(left, oper, right),
            _ => {
                return Err(format_error(
                    oper,
                    &format!("Invalid comparison operator: {}", oper),
                ))
            }
        };

        Ok(Value {
            inner: Rc::new(result),
            moo_type: MooType::Bool,
        })
    }

    fn evaluate_cast(&mut self, value: Value, cast_type: &Token) -> Result<Value, Error> {
        match MooType::from_token(cast_type)? {
            MooType::Int8 => {
                let casted_value: i8 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()?,
                    MooType::Int16 => value.as_type::<i16>()? as i8,
                    MooType::Int32 => value.as_type::<i32>()? as i8,
                    MooType::Int64 => value.as_type::<i64>()? as i8,
                    MooType::Uint8 => value.as_type::<u8>()? as i8,
                    MooType::Uint16 => value.as_type::<u16>()? as i8,
                    MooType::Uint32 => value.as_type::<u32>()? as i8,
                    MooType::Uint64 => value.as_type::<u64>()? as i8,
                    MooType::Float32 => value.as_type::<f32>()? as i8,
                    MooType::Float64 => value.as_type::<f64>()? as i8,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Int8,
                })
            }
            MooType::Int16 => {
                let casted_value: i16 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as i16,
                    MooType::Int16 => value.as_type::<i16>()?,
                    MooType::Int32 => value.as_type::<i32>()? as i16,
                    MooType::Int64 => value.as_type::<i64>()? as i16,
                    MooType::Uint8 => value.as_type::<u8>()? as i16,
                    MooType::Uint16 => value.as_type::<u16>()? as i16,
                    MooType::Uint32 => value.as_type::<u32>()? as i16,
                    MooType::Uint64 => value.as_type::<u64>()? as i16,
                    MooType::Float32 => value.as_type::<f32>()? as i16,
                    MooType::Float64 => value.as_type::<f64>()? as i16,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Int16,
                })
            }
            MooType::Int32 => {
                let casted_value: i32 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as i32,
                    MooType::Int16 => value.as_type::<i16>()? as i32,
                    MooType::Int32 => value.as_type::<i32>()?,
                    MooType::Int64 => value.as_type::<i64>()? as i32,
                    MooType::Uint8 => value.as_type::<u8>()? as i32,
                    MooType::Uint16 => value.as_type::<u16>()? as i32,
                    MooType::Uint32 => value.as_type::<u32>()? as i32,
                    MooType::Uint64 => value.as_type::<u64>()? as i32,
                    MooType::Float32 => value.as_type::<f32>()? as i32,
                    MooType::Float64 => value.as_type::<f64>()? as i32,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Int32,
                })
            }
            MooType::Int64 => {
                let casted_value: i64 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as i64,
                    MooType::Int16 => value.as_type::<i16>()? as i64,
                    MooType::Int32 => value.as_type::<i32>()? as i64,
                    MooType::Int64 => value.as_type::<i64>()?,
                    MooType::Uint8 => value.as_type::<u8>()? as i64,
                    MooType::Uint16 => value.as_type::<u16>()? as i64,
                    MooType::Uint32 => value.as_type::<u32>()? as i64,
                    MooType::Uint64 => value.as_type::<u64>()? as i64,
                    MooType::Float32 => value.as_type::<f32>()? as i64,
                    MooType::Float64 => value.as_type::<f64>()? as i64,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Int64,
                })
            }
            MooType::Uint8 => {
                let casted_value: u8 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as u8,
                    MooType::Int16 => value.as_type::<i16>()? as u8,
                    MooType::Int32 => value.as_type::<i32>()? as u8,
                    MooType::Int64 => value.as_type::<i64>()? as u8,
                    MooType::Uint8 => value.as_type::<u8>()?,
                    MooType::Uint16 => value.as_type::<u16>()? as u8,
                    MooType::Uint32 => value.as_type::<u32>()? as u8,
                    MooType::Uint64 => value.as_type::<u64>()? as u8,
                    MooType::Float32 => value.as_type::<f32>()? as u8,
                    MooType::Float64 => value.as_type::<f64>()? as u8,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Uint8,
                })
            }
            MooType::Uint16 => {
                let casted_value: u16 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as u16,
                    MooType::Int16 => value.as_type::<i16>()? as u16,
                    MooType::Int32 => value.as_type::<i32>()? as u16,
                    MooType::Int64 => value.as_type::<i64>()? as u16,
                    MooType::Uint8 => value.as_type::<u8>()? as u16,
                    MooType::Uint16 => value.as_type::<u16>()?,
                    MooType::Uint32 => value.as_type::<u32>()? as u16,
                    MooType::Uint64 => value.as_type::<u64>()? as u16,
                    MooType::Float32 => value.as_type::<f32>()? as u16,
                    MooType::Float64 => value.as_type::<f64>()? as u16,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Uint16,
                })
            }
            MooType::Uint32 => {
                let casted_value: u32 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as u32,
                    MooType::Int16 => value.as_type::<i16>()? as u32,
                    MooType::Int32 => value.as_type::<i32>()? as u32,
                    MooType::Int64 => value.as_type::<i64>()? as u32,
                    MooType::Uint8 => value.as_type::<u8>()? as u32,
                    MooType::Uint16 => value.as_type::<u16>()? as u32,
                    MooType::Uint32 => value.as_type::<u32>()?,
                    MooType::Uint64 => value.as_type::<u64>()? as u32,
                    MooType::Float32 => value.as_type::<f32>()? as u32,
                    MooType::Float64 => value.as_type::<f64>()? as u32,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Uint32,
                })
            }
            MooType::Uint64 => {
                let casted_value: u64 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as u64,
                    MooType::Int16 => value.as_type::<i16>()? as u64,
                    MooType::Int32 => value.as_type::<i32>()? as u64,
                    MooType::Int64 => value.as_type::<i64>()? as u64,
                    MooType::Uint8 => value.as_type::<u8>()? as u64,
                    MooType::Uint16 => value.as_type::<u16>()? as u64,
                    MooType::Uint32 => value.as_type::<u32>()? as u64,
                    MooType::Uint64 => value.as_type::<u64>()?,
                    MooType::Float32 => value.as_type::<f32>()? as u64,
                    MooType::Float64 => value.as_type::<f64>()? as u64,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Uint64,
                })
            }
            MooType::Float32 => {
                let casted_value: f32 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as f32,
                    MooType::Int16 => value.as_type::<i16>()? as f32,
                    MooType::Int32 => value.as_type::<i32>()? as f32,
                    MooType::Int64 => value.as_type::<i64>()? as f32,
                    MooType::Uint8 => value.as_type::<u8>()? as f32,
                    MooType::Uint16 => value.as_type::<u16>()? as f32,
                    MooType::Uint32 => value.as_type::<u32>()? as f32,
                    MooType::Uint64 => value.as_type::<u64>()? as f32,
                    MooType::Float32 => value.as_type::<f32>()?,
                    MooType::Float64 => value.as_type::<f64>()? as f32,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Float32,
                })
            }
            MooType::Float64 => {
                let casted_value: f64 = match value.moo_type {
                    MooType::Int8 => value.as_type::<i8>()? as f64,
                    MooType::Int16 => value.as_type::<i16>()? as f64,
                    MooType::Int32 => value.as_type::<i32>()? as f64,
                    MooType::Int64 => value.as_type::<i64>()? as f64,
                    MooType::Uint8 => value.as_type::<u8>()? as f64,
                    MooType::Uint16 => value.as_type::<u16>()? as f64,
                    MooType::Uint32 => value.as_type::<u32>()? as f64,
                    MooType::Uint64 => value.as_type::<u64>()? as f64,
                    MooType::Float32 => value.as_type::<f32>()? as f64,
                    MooType::Float64 => value.as_type::<f64>()?,
                    _ => {
                        return Err(format_error(
                            cast_type,
                            &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                        ))
                    }
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::Float64,
                })
            }
            MooType::String => {
                let casted_value: String = match value.moo_type {
                    MooType::Int8 => format!("{}", value.as_type::<i8>()?),
                    MooType::Int16 => format!("{}", value.as_type::<i16>()?),
                    MooType::Int32 => format!("{}", value.as_type::<i32>()?),
                    MooType::Int64 => format!("{}", value.as_type::<i64>()?),
                    MooType::Uint8 => format!("{}", value.as_type::<u8>()?),
                    MooType::Uint16 => format!("{}", value.as_type::<u16>()?),
                    MooType::Uint32 => format!("{}", value.as_type::<u32>()?),
                    MooType::Uint64 => format!("{}", value.as_type::<u64>()?),
                    MooType::Float32 => format!("{}", value.as_type::<f32>()?),
                    MooType::Float64 => format!("{}", value.as_type::<f64>()?),
                    MooType::String => value.as_type::<String>()?,
                    MooType::Bool => format!("{}", value.as_type::<bool>()?),
                };
                Ok(Value {
                    inner: Rc::new(casted_value),
                    moo_type: MooType::String,
                })
            }
            _ => {
                return Err(format_error(
                    cast_type,
                    &format!("Cannot cast from {} to {}", value.moo_type, cast_type),
                ))
            }
        }
    }

    fn evaluate_unary(&mut self, oper: &Token, right: Value) -> Result<Value, Error> {
        if right.moo_type == MooType::Float32 {
            if_chain! {
                if let Token::Minus { loc: _ } = oper;
                then {
                    let number = right.as_type::<f32>()?;
                    Ok(Value {
                        inner: Rc::new(-number),
                        moo_type: MooType::Float32,
                    })
                } else {
                    Err(format_error(oper, &format!("Only - is supported as a unary operator with numerical types")))
                }
            }
        } else if right.moo_type == MooType::Bool {
            if_chain! {
                if let Token::Bang { loc: _ } = oper;
                then {
                    let bool_value = right.as_type::<bool>()?;
                    Ok(Value {
                        inner: Rc::new(!bool_value),
                        moo_type: MooType::Bool,
                    })
                } else {
                    Err(format_error(oper, &format!("Only ! is supported as a unary operator with bool types")))
                }
            }
        } else {
            Err(format_error(
                oper,
                &format!("Unary operators are only valid with numerical or bool types"),
            ))
        }
    }

    fn evaluate_string(&mut self, token: &Token) -> Result<Value, Error> {
        match token {
            Token::StringLiteral { loc: _, lexeme } => Ok(Value {
                inner: Rc::new(lexeme.to_owned()),
                moo_type: MooType::String,
            }),
            _ => Err(format_error(token, "Expected string literal")),
        }
    }

    fn evaluate_number(&mut self, token: &Token) -> Result<Value, Error> {
        match token {
            Token::Number { loc: _, lexeme } => {
                if lexeme.contains(".") {
                    if let Ok(number) = lexeme.parse::<f32>() {
                        Ok(Value {
                            inner: Rc::new(number),
                            moo_type: MooType::Float32,
                        })
                    } else {
                        Err(format_error(
                            token,
                            &format!("Failed to parse a number literal from {}", lexeme),
                        ))
                    }
                } else {
                    if let Ok(number) = lexeme.parse::<i32>() {
                        Ok(Value {
                            inner: Rc::new(number),
                            moo_type: MooType::Int32,
                        })
                    } else {
                        Err(format_error(
                            token,
                            &format!("Failed to parse a number literal from {}", lexeme),
                        ))
                    }
                }
            }
            _ => Err(format_error(token, "Expected number literal")),
        }
    }
}
