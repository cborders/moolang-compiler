mod definer;
mod environment;
mod evaluator;
mod value;

use {
    crate::{
        errors::format_error,
        lexer::Token,
        parser::{ConditionalStatement, Expr, Stmt},
        program::{File, Program},
        types::MooType,
    },
    anyhow::{anyhow, Error},
    environment::{Environment, Scope},
    evaluator::Evaluator,
    if_chain::if_chain,
    std::rc::Rc,
    value::Value,
};

pub fn execute(program: &Program) -> Result<(), Vec<Error>> {
    let mut environment = Environment::new();
    definer::define(program, &mut environment)?;

    let mut executer = Executer::new(program, &environment);
    if let Err(error) = executer.execute() {
        return Err(vec![error]);
    }

    Ok(())
}

#[derive(PartialEq)]
enum ScopeType {
    Global,
    File,
    Named,
}

struct Executer<'a> {
    program: &'a Program,
    env: &'a Environment,
    curr_file: &'a File,
    curr_scope: Rc<Scope>,
    scope_type: ScopeType,
}

impl<'a> Executer<'a> {
    fn new(program: &'a Program, environment: &'a Environment) -> Self {
        let curr_file = &program.files[0];
        let file_scope = environment.file_scope(&curr_file.path);

        Self {
            program,
            env: environment,
            curr_file,
            curr_scope: file_scope,
            scope_type: ScopeType::File,
        }
    }

    pub fn execute(&mut self) -> Result<(), Error> {
        for file in &self.program.files {
            for stmt in &file.stmts {
                self.execute_stmt(&stmt)?;
            }
        }

        Ok(())
    }

    fn execute_stmt(&mut self, stmt: &Stmt) -> Result<(), Error> {
        match stmt {
            Stmt::Expr { expr } => self.expression_statement(expr),
            Stmt::Print { expr } => self.print_statement(expr),
            Stmt::Scope { token } => self.scope_statement(token),
            Stmt::VarDecl {
                ident,
                constant,
                var_type,
                expr,
            } => self.var_declaration(ident, constant, var_type, expr),
            Stmt::When {
                conditions,
                else_stmt,
            } => self.when_statement(conditions, else_stmt),
            Stmt::While { condition, block } => self.while_statement(condition, block),
            Stmt::Block { stmts } => {
                let old_scope = Rc::clone(&self.curr_scope);
                self.curr_scope = Rc::new(self.curr_scope.push_new());

                for stmt in stmts {
                    self.execute_stmt(stmt)?;
                }

                self.curr_scope = old_scope;
                Ok(())
            }
        }
    }

    fn expression_statement(&mut self, expr: &Expr) -> Result<(), Error> {
        let _ = self.evaluate(expr)?;
        Ok(())
    }

    fn print_statement(&mut self, expr: &Expr) -> Result<(), Error> {
        let value = self.evaluate(expr)?;
        println!("{}", value);

        Ok(())
    }

    fn scope_statement(&mut self, token: &Token) -> Result<(), Error> {
        match token {
            Token::GlobalScope { loc: _ } => {
                self.curr_scope = Rc::clone(&self.env.global_scope);
                self.scope_type = ScopeType::Global;
            }
            Token::FileScope { loc: _ } => {
                self.curr_scope = self.env.file_scope(&self.curr_file.path);
                self.scope_type = ScopeType::File;
            }
            Token::NamedScope { loc: _, lexeme } => {
                self.scope_type = ScopeType::Named;
                let named_scope = self.env.named_scope(lexeme);
                named_scope.reparent(&self.env.file_scope(&self.curr_file.path));
                self.curr_scope = named_scope;
            }
            _ => return Err(anyhow!("Expected a scope, got {}", token)),
        }
        Ok(())
    }

    fn var_declaration(
        &mut self,
        ident: &Token,
        constant: &bool,
        var_type: &Option<Token>,
        expr: &Option<Box<Expr>>,
    ) -> Result<(), Error> {
        if let Token::Ident { loc, lexeme } = ident {
            // If we are in global scope this variable should have been defined
            // in the definition pass so let's just verify that it's there.
            if_chain! {
                if self.scope_type == ScopeType::Global || self.scope_type == ScopeType::Named;
                if let Some(info) = self.curr_scope.get_info(lexeme);
                if info.decl_loc == *loc;
                then {
                    // This is the same variable that's already defined
                    // so we can just stop here
                    return Ok(());
                }
            }

            let value = if let Some(expr) = expr {
                Some(self.evaluate(expr)?)
            } else {
                None
            };

            if let Err(error) = self.curr_scope.define(ident, constant, value) {
                Err(format_error(ident, &format!("{:?}", error)))
            } else {
                Ok(())
            }
        } else {
            Err(format_error(ident, "Expected identifier"))
        }
    }

    fn when_statement(
        &mut self,
        conditions: &Vec<ConditionalStatement>,
        else_stmt: &Option<Box<Stmt>>,
    ) -> Result<(), Error> {
        for condition in conditions {
            let value = self.evaluate(&condition.condition)?;
            if value.moo_type != MooType::Bool {
                return Err(format_error(
                    condition.condition.token(),
                    "expected a boolean expression",
                ));
            }

            if value.as_type::<bool>().ok().unwrap() {
                return self.execute_stmt(&condition.statement);
            }
        }

        if let Some(else_stmt) = else_stmt {
            self.execute_stmt(else_stmt)
        } else {
            Ok(())
        }
    }

    fn while_statement(&mut self, condition: &Expr, block: &Stmt) -> Result<(), Error> {
        loop {
            let value = self.evaluate(condition)?;
            if value.moo_type != MooType::Bool {
                return Err(format_error(
                    condition.token(),
                    "expected a boolean expression",
                ));
            }

            if !value.as_type::<bool>().ok().unwrap() {
                break;
            }

            self.execute_stmt(block)?;
        }

        Ok(())
    }
}

impl<'a> Evaluator for Executer<'a> {
    fn evaluate_assignment(&mut self, ident: &Token, value: Value) -> Result<Value, Error> {
        if let Token::Ident { loc: _, lexeme } = ident {
            match self.curr_scope.set(lexeme, value.clone()) {
                Ok(()) => Ok(value),
                Err(error) => Err(format_error(ident, &format!("{:?}", error))),
            }
        } else {
            Err(format_error(ident, "Expected identifier"))
        }
    }

    fn evaluate_variable(&mut self, ident: &Token) -> Result<Value, Error> {
        if let Token::Ident { loc: _, lexeme } = ident {
            match self.curr_scope.get(lexeme) {
                Ok(value) => Ok(value),
                Err(error) => Err(format_error(ident, &format!("{:?}", error))),
            }
        } else {
            Err(format_error(ident, "Expected identifier"))
        }
    }
}
