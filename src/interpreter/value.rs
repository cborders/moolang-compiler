use {
    crate::types::MooType,
    anyhow::{anyhow, Error},
    std::{
        any::{Any, TypeId},
        fmt,
        rc::Rc,
    },
};

#[derive(Clone)]
pub struct Value {
    pub(crate) inner: Rc<dyn Any>,
    pub(crate) moo_type: MooType,
}

impl Value {
    pub fn as_type<T: 'static + Clone>(&self) -> Result<T, Error> {
        if let Some(value) = self.inner.downcast_ref::<T>() {
            Ok(value.clone())
        } else {
            Err(anyhow! {"Cannot convert {:?} to {:?}", self.type_id(), std::any::type_name::<T>()})
        }
    }

    fn type_id(&self) -> TypeId {
        (&*self.inner).type_id()
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.moo_type {
            MooType::Int8 => write!(f, "{}", self.as_type::<i8>().ok().unwrap()),
            MooType::Int16 => write!(f, "{}", self.as_type::<i16>().ok().unwrap()),
            MooType::Int32 => write!(f, "{}", self.as_type::<i32>().ok().unwrap()),
            MooType::Int64 => write!(f, "{}", self.as_type::<i64>().ok().unwrap()),
            MooType::Uint8 => write!(f, "{}", self.as_type::<u8>().ok().unwrap()),
            MooType::Uint16 => write!(f, "{}", self.as_type::<u16>().ok().unwrap()),
            MooType::Uint32 => write!(f, "{}", self.as_type::<u32>().ok().unwrap()),
            MooType::Uint64 => write!(f, "{}", self.as_type::<u64>().ok().unwrap()),
            MooType::Float32 => write!(f, "{}", self.as_type::<f32>().ok().unwrap()),
            MooType::Float64 => write!(f, "{}", self.as_type::<f64>().ok().unwrap()),
            MooType::String => write!(f, "{}", self.as_type::<String>().ok().unwrap()),
            MooType::Bool => write!(f, "{}", self.as_type::<bool>().ok().unwrap()),
        }
    }
}

impl fmt::Debug for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.moo_type {
            MooType::Int8 => write!(f, "{}", self.as_type::<i8>().ok().unwrap()),
            MooType::Int16 => write!(f, "{}", self.as_type::<i16>().ok().unwrap()),
            MooType::Int32 => write!(f, "{}", self.as_type::<i32>().ok().unwrap()),
            MooType::Int64 => write!(f, "{}", self.as_type::<i64>().ok().unwrap()),
            MooType::Uint8 => write!(f, "{}", self.as_type::<u8>().ok().unwrap()),
            MooType::Uint16 => write!(f, "{}", self.as_type::<u16>().ok().unwrap()),
            MooType::Uint32 => write!(f, "{}", self.as_type::<u32>().ok().unwrap()),
            MooType::Uint64 => write!(f, "{}", self.as_type::<u64>().ok().unwrap()),
            MooType::Float32 => write!(f, "{}", self.as_type::<f32>().ok().unwrap()),
            MooType::Float64 => write!(f, "{}", self.as_type::<f64>().ok().unwrap()),
            MooType::String => write!(f, "{}", self.as_type::<String>().ok().unwrap()),
            MooType::Bool => write!(f, "{}", self.as_type::<bool>().ok().unwrap()),
        }
    }
}
