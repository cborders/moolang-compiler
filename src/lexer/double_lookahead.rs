use std::fmt;

pub trait DoubleLookAhead {
    fn curr(&self) -> char;
    fn next(&self) -> Option<char>;
    fn after_next(&self) -> Option<char>;
    fn advance(&mut self);
    fn loc(&self) -> Location;
}

#[derive(Clone, Debug, PartialEq)]
pub struct Location {
    pub file: String,
    pub line: i32,
    pub col: i32,
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} [{}:{}]", self.file, self.line, self.col)
    }
}
