use {
    crate::lexer::{DoubleLookAhead, Location},
    anyhow::{anyhow, Error},
    std::{
        fs::File,
        io::{prelude::*, BufReader},
    },
};

pub struct FileStream {
    reader: BufReader<File>,
    curr: char,
    next: Option<char>,
    after_next: Option<char>,
    file: String,
    line: i32,
    col: i32,
}

impl FileStream {
    pub fn new(file_path: &str) -> Result<Self, Error> {
        let file = File::open(file_path)?;
        let mut reader = BufReader::new(file);

        let mut chars = [0; 3];
        let read = reader.read(&mut chars)?;

        if read == 0 {
            return Err(anyhow!("Failed to read file: {}", file_path));
        }

        let curr = chars[0] as char;
        let next = if read >= 2 {
            Some(chars[1] as char)
        } else {
            None
        };
        let after_next = if read >= 3 {
            Some(chars[2] as char)
        } else {
            None
        };

        Ok(Self {
            reader,
            curr,
            next,
            after_next,
            file: String::from(file_path),
            line: 1,
            col: 1,
        })
    }

    fn read(&mut self) -> Option<char> {
        // At this point the file is open and some charaters have already been read from it so if
        // this fails there's really not much to be done for it.
        let mut chars = [0; 1];
        let read = self.reader.read(&mut chars).expect("IO error occured");

        if read == 1 {
            Some(chars[0] as char)
        } else {
            None
        }
    }
}

impl DoubleLookAhead for FileStream {
    fn curr(&self) -> char {
        self.curr
    }

    fn next(&self) -> Option<char> {
        self.next
    }

    fn after_next(&self) -> Option<char> {
        self.after_next
    }

    fn advance(&mut self) {
        if self.curr == '\n' {
            self.line += 1;
            self.col = 0;
        }

        self.col += 1;

        self.curr = self.next.unwrap_or('\0');
        self.next = self.after_next;
        self.after_next = self.read();

        // Remove Windows line-feed characters
        if let Some('\r') = self.after_next {
            self.after_next = self.read();
        }
    }

    fn loc(&self) -> Location {
        Location {
            file: self.file.to_owned(),
            line: self.line,
            col: self.col,
        }
    }
}
