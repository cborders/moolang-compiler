mod double_lookahead;
mod tokens;

pub mod file_stream;
pub mod string_stream;

pub use {
    crate::program::{File, Program},
    double_lookahead::{DoubleLookAhead, Location},
    file_stream::FileStream,
    std::rc::Rc,
    tokens::Token,
};

use {
    anyhow::{anyhow, Error},
    if_chain::if_chain,
};

pub fn lex(paths: Vec<String>, program: &mut Program) -> Result<(), Vec<Error>> {
    let mut errors = vec![];
    for path in paths {
        match FileStream::new(&path) {
            Ok(stream) => match scan(stream) {
                Ok(tokens) => {
                    let mut file = File::new(path.to_owned());
                    file.tokens = tokens;
                    program.files.push(file);
                }
                Err(mut errs) => errors.append(&mut errs),
            },
            Err(error) => return Err(vec![error]),
        }
    }

    if errors.len() > 0 {
        Err(errors)
    } else {
        Ok(())
    }
}

fn scan(mut stream: impl DoubleLookAhead) -> Result<Vec<Token>, Vec<Error>> {
    let mut tokens: Vec<Token> = vec![];
    let mut errors: Vec<Error> = vec![];

    while stream.curr() != '\0' {
        eat_whitespace(&mut stream);

        let loc = stream.loc();
        let result = match stream.curr() {
            '\n' => Some(Ok(Token::EOL { loc })),
            '(' => Some(Ok(Token::LParen { loc })),
            ')' => Some(Ok(Token::RParen { loc })),
            '[' => Some(Ok(Token::LBracket { loc })),
            ']' => Some(Ok(Token::RBracket { loc })),
            '{' => Some(Ok(Token::LBrace { loc })),
            '}' => Some(Ok(Token::RBrace { loc })),
            ':' => Some(Ok(Token::Colon { loc })),
            ',' => Some(Ok(Token::Comma { loc })),
            '"' => Some(Ok(scan_string_literal(loc, &mut stream))),
            '@' => {
                if_chain! {
                    if let Some(c) = stream.next();
                    if c.is_alphabetic();
                    then {
                        Some(Ok(scan_scope(loc, &mut stream)))
                    } else {
                        Some(Ok(Token::At { loc }))
                    }
                }
            }
            '.' => {
                if let Some('.') = stream.next() {
                    if let Some('.') = stream.after_next() {
                        stream.advance();
                        stream.advance();
                        Some(Ok(Token::RangeInc { loc }))
                    } else if let Some('<') = stream.after_next() {
                        stream.advance();
                        stream.advance();
                        Some(Ok(Token::RangeEx { loc }))
                    } else {
                        Some(Ok(Token::Dot { loc }))
                    }
                } else {
                    Some(Ok(Token::Dot { loc }))
                }
            }
            '=' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::EqualEqual { loc }))
                } else {
                    Some(Ok(Token::Equal { loc }))
                }
            }
            '|' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::PipeEqual { loc }))
                } else {
                    Some(Ok(Token::Pipe { loc }))
                }
            }
            '&' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::AmpersandEqual { loc }))
                } else {
                    Some(Ok(Token::Ampersand { loc }))
                }
            }
            '!' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::BangEqual { loc }))
                } else {
                    Some(Ok(Token::Bang { loc }))
                }
            }
            '+' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::PlusEqual { loc }))
                } else {
                    Some(Ok(Token::Plus { loc }))
                }
            }
            '-' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::MinusEqual { loc }))
                } else {
                    Some(Ok(Token::Minus { loc }))
                }
            }
            '*' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::StarEqual { loc }))
                } else {
                    Some(Ok(Token::Star { loc }))
                }
            }
            '/' => match stream.next() {
                Some('=') => {
                    stream.advance();
                    Some(Ok(Token::SlashEqual { loc }))
                }
                Some('/') => {
                    eat_single_line_comment(&mut stream);
                    None
                }
                Some('*') => {
                    eat_multi_line_comment(&mut stream);
                    None
                }
                _ => Some(Ok(Token::Slash { loc })),
            },
            '%' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::PercentEqual { loc }))
                } else {
                    Some(Ok(Token::Percent { loc }))
                }
            }
            '>' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::GreaterEqual { loc }))
                } else {
                    Some(Ok(Token::Greater { loc }))
                }
            }
            '<' => {
                if let Some('=') = stream.next() {
                    stream.advance();
                    Some(Ok(Token::LessEqual { loc }))
                } else {
                    Some(Ok(Token::Less { loc }))
                }
            }
            _ => {
                if stream.curr().is_alphabetic() || stream.curr() == '_' {
                    Some(Ok(scan_ident(loc, &mut stream)))
                } else if stream.curr().is_digit(10) {
                    Some(Ok(scan_number(loc, &mut stream)))
                } else {
                    Some(Err(anyhow!(
                        "{} [{}:{}] {}",
                        loc.file,
                        loc.line,
                        loc.col,
                        format!("Unknown token: {}", stream.curr())
                    )))
                }
            }
        };

        if result.is_none() {
            continue;
        }

        match result.unwrap() {
            Ok(token) => tokens.push(token),
            Err(error) => errors.push(error),
        }

        stream.advance();
    }

    tokens.push(Token::EOL { loc: stream.loc() });

    if errors.len() > 0 {
        Err(errors)
    } else {
        Ok(tokens)
    }
}

fn eat_whitespace(stream: &mut impl DoubleLookAhead) {
    while stream.curr() == ' ' {
        stream.advance();
    }
}

fn scan_ident(loc: Location, stream: &mut impl DoubleLookAhead) -> Token {
    let mut chars: Vec<char> = vec![stream.curr()];
    while let Some(c) = stream.next() {
        if !c.is_alphanumeric() && c != '_' {
            break;
        }

        chars.push(c);
        stream.advance();
    }

    let lexeme = chars.into_iter().collect();
    check_for_keywords(loc, lexeme)
}

fn scan_scope(loc: Location, stream: &mut impl DoubleLookAhead) -> Token {
    // Pop off '@'
    stream.advance();

    // Get scope name
    let mut chars: Vec<char> = vec![stream.curr()];
    while let Some(c) = stream.next() {
        if !c.is_alphanumeric() && c != '_' {
            break;
        }

        chars.push(c);
        stream.advance();
    }

    let lexeme: String = chars.into_iter().collect();
    match lexeme.as_ref() {
        "global" => Token::GlobalScope { loc },
        "file" => Token::FileScope { loc },
        _ => Token::NamedScope { loc, lexeme },
    }
}

fn scan_number(loc: Location, stream: &mut impl DoubleLookAhead) -> Token {
    let mut chars: Vec<char> = vec![stream.curr()];
    loop {
        if_chain! {
            if let Some(c) = stream.next();
            if c.is_digit(10) || c == '_';
            then {
                chars.push(c);
                stream.advance()
            } else {
                break;
            }
        }
    }

    // Ran out of digits and underscore so see if the next char is a '.'
    if_chain! {
        if let Some(c) = stream.next();
        if c == '.';
        if let Some(after_c) = stream.after_next();
        if after_c.is_digit(10) || after_c == '_';
        then {
            // It's a decimal point
            chars.push(c);
            stream.advance();

            // Read the rest of the number
            loop {
                if_chain! {
                    if let Some(c) = stream.next();
                    if c.is_digit(10) || c == '_';
                    then {
                        chars.push(c);
                        stream.advance()
                    } else {
                        break;
                    }
                }
            }
        }
    }

    let lexeme = chars.into_iter().collect();
    Token::Number { loc, lexeme }
}

fn scan_string_literal(loc: Location, stream: &mut impl DoubleLookAhead) -> Token {
    // Check to see if there this is an empty string
    if let Some('"') = stream.next() {
        // Pop off the two quotes and return an empty string.
        stream.advance();
        stream.advance();

        Token::StringLiteral {
            loc,
            lexeme: String::new(),
        }
    } else {
        // Pop starting double quote
        stream.advance();

        let mut chars: Vec<char> = vec![];
        while let Some(c) = stream.next() {
            chars.push(stream.curr());
            if c == '"' {
                break;
            }
            stream.advance();
        }

        // Move ending double quote into place
        stream.advance();

        let lexeme = chars.into_iter().collect();
        Token::StringLiteral { loc, lexeme }
    }
}

fn eat_single_line_comment(stream: &mut impl DoubleLookAhead) {
    // Eat everything up to the EOL
    while stream.curr() != '\n' {
        stream.advance();
    }
    // Eat the EOL
    stream.advance();
}

// TODO(Casey): Support nested block comments
fn eat_multi_line_comment(stream: &mut impl DoubleLookAhead) {
    // Eat everything until we get to a */
    loop {
        if_chain! {
            if let Some('*') = stream.next();
            if let Some('/') = stream.after_next();
            then {
                break
            } else {
                stream.advance();
            }
        }
    }

    // Eat the last character and the */
    stream.advance();
    stream.advance();
    stream.advance();
}

fn check_for_keywords(loc: Location, lexeme: String) -> Token {
    match lexeme.as_ref() {
        "class" => Token::Class { loc },
        "fun" => Token::Fun { loc },
        "var" => Token::Var { loc },
        "let" => Token::Let { loc },
        "for" => Token::For { loc },
        "in" => Token::In { loc },
        "when" => Token::When { loc },
        "else" => Token::Else { loc },
        "return" => Token::Return { loc },
        "while" => Token::While { loc },
        "as" => Token::As { loc },
        "int8" => Token::Int8 { loc },
        "int16" => Token::Int16 { loc },
        "int32" => Token::Int32 { loc },
        "int64" => Token::Int64 { loc },
        "uint8" => Token::Uint8 { loc },
        "uint16" => Token::Uint16 { loc },
        "uint32" => Token::Uint32 { loc },
        "uint64" => Token::Uint64 { loc },
        "float32" => Token::Float32 { loc },
        "float64" => Token::Float64 { loc },
        "string" => Token::StringType { loc },
        "false" => Token::False { loc },
        "true" => Token::True { loc },
        "null" => Token::Null { loc },
        "print" => Token::Print { loc },
        _ => Token::Ident { loc, lexeme },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct TestStream {
        data: String,
        index: usize,
        file: String,
        line: i32,
        col: i32,
    }

    impl TestStream {
        pub fn new(file_path: &str, code: String) -> Self {
            Self {
                data: code,
                index: 0,
                file: String::from(file_path),
                line: 1,
                col: 1,
            }
        }

        fn get_nth(&self, index: usize) -> Option<char> {
            if index < self.data.len() {
                Some(self.data.chars().nth(index).unwrap())
            } else {
                None
            }
        }
    }

    impl DoubleLookAhead for TestStream {
        fn curr(&self) -> char {
            if self.index < self.data.len() {
                self.data.chars().nth(self.index).unwrap()
            } else {
                '\0'
            }
        }

        fn next(&self) -> Option<char> {
            self.get_nth(self.index + 1)
        }

        fn after_next(&self) -> Option<char> {
            self.get_nth(self.index + 2)
        }

        fn advance(&mut self) {
            if self.curr() == '\n' {
                self.line += 1;
                self.col = 0;
            }

            self.index += 1;
            self.col += 1;
        }

        fn loc(&self) -> Location {
            Location {
                file: self.file.to_owned(),
                line: self.line,
                col: self.col,
            }
        }
    }

    fn assert_tokens(actual: &Vec<Token>, expected: &Vec<Token>) {
        assert_eq!(
            actual.len(),
            expected.len(),
            "Expected {} tokens but got {}",
            expected.len(),
            actual.len()
        );

        for index in 0..actual.len() {
            assert_eq!(&actual[index], &expected[index]);
        }
    }

    fn assert_errors(actual: &Vec<Error>, expected: &Vec<Error>) {
        assert_eq!(
            actual.len(),
            expected.len(),
            "Expected {} errors but got {}",
            expected.len(),
            actual.len()
        );

        for index in 0..actual.len() {
            let actual_msg = format!("{:?}", &actual[index]);
            let expected_msg = format!("{:?}", &expected[index]);
            assert_eq!(actual_msg, expected_msg);
        }
    }

    macro_rules! make_loc {
        ($file:expr, $line:expr, $col:expr) => {
            Location {
                file: String::from($file),
                line: $line,
                col: $col,
            }
        };
    }

    #[test]
    fn test_that_keywords_are_found() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("fun var let for in when else return while as int8 int16 int32 int64 float32 float64 string false true null class"));
        let expected = vec![
            Token::Fun {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::Var {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::Let {
                loc: make_loc!(file_name, 1, 9),
            },
            Token::For {
                loc: make_loc!(file_name, 1, 13),
            },
            Token::In {
                loc: make_loc!(file_name, 1, 17),
            },
            Token::When {
                loc: make_loc!(file_name, 1, 20),
            },
            Token::Else {
                loc: make_loc!(file_name, 1, 25),
            },
            Token::Return {
                loc: make_loc!(file_name, 1, 30),
            },
            Token::While {
                loc: make_loc!(file_name, 1, 37),
            },
            Token::As {
                loc: make_loc!(file_name, 1, 43),
            },
            Token::Int8 {
                loc: make_loc!(file_name, 1, 46),
            },
            Token::Int16 {
                loc: make_loc!(file_name, 1, 51),
            },
            Token::Int32 {
                loc: make_loc!(file_name, 1, 57),
            },
            Token::Int64 {
                loc: make_loc!(file_name, 1, 63),
            },
            Token::Float32 {
                loc: make_loc!(file_name, 1, 69),
            },
            Token::Float64 {
                loc: make_loc!(file_name, 1, 77),
            },
            Token::StringType {
                loc: make_loc!(file_name, 1, 85),
            },
            Token::False {
                loc: make_loc!(file_name, 1, 92),
            },
            Token::True {
                loc: make_loc!(file_name, 1, 98),
            },
            Token::Null {
                loc: make_loc!(file_name, 1, 103),
            },
            Token::Class {
                loc: make_loc!(file_name, 1, 108),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 113),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_mix_idents_and_keywords() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("fun in the sun for me"));
        let expected = vec![
            Token::Fun {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::In {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 8),
                lexeme: String::from("the"),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 12),
                lexeme: String::from("sun"),
            },
            Token::For {
                loc: make_loc!(file_name, 1, 16),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 20),
                lexeme: String::from("me"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 22),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_int_literal() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("123 1_234 1_234_567_890"));
        let expected = vec![
            Token::Number {
                loc: make_loc!(file_name, 1, 1),
                lexeme: String::from("123"),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 5),
                lexeme: String::from("1_234"),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 11),
                lexeme: String::from("1_234_567_890"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 24),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_int_literal_dot_operator() {
        let file_name = "TestFile";
        let stream = TestStream::new(
            file_name,
            String::from("123.round() 1_234.add() 1_234_567_890.big()"),
        );
        let expected = vec![
            Token::Number {
                loc: make_loc!(file_name, 1, 1),
                lexeme: String::from("123"),
            },
            Token::Dot {
                loc: make_loc!(file_name, 1, 4),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 5),
                lexeme: String::from("round"),
            },
            Token::LParen {
                loc: make_loc!(file_name, 1, 10),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 11),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 13),
                lexeme: String::from("1_234"),
            },
            Token::Dot {
                loc: make_loc!(file_name, 1, 18),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 19),
                lexeme: String::from("add"),
            },
            Token::LParen {
                loc: make_loc!(file_name, 1, 22),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 23),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 25),
                lexeme: String::from("1_234_567_890"),
            },
            Token::Dot {
                loc: make_loc!(file_name, 1, 38),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 39),
                lexeme: String::from("big"),
            },
            Token::LParen {
                loc: make_loc!(file_name, 1, 42),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 43),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 44),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_float_literal() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("1.23 1_234.5 1_234.567_890"));
        let expected = vec![
            Token::Number {
                loc: make_loc!(file_name, 1, 1),
                lexeme: String::from("1.23"),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 6),
                lexeme: String::from("1_234.5"),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 14),
                lexeme: String::from("1_234.567_890"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 27),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_float_literal_dot_operator() {
        let file_name = "TestFile";
        let stream = TestStream::new(
            file_name,
            String::from("1.23.round() 1_234.5.add() 1_234.567_890.big()"),
        );
        let expected = vec![
            Token::Number {
                loc: make_loc!(file_name, 1, 1),
                lexeme: String::from("1.23"),
            },
            Token::Dot {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 6),
                lexeme: String::from("round"),
            },
            Token::LParen {
                loc: make_loc!(file_name, 1, 11),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 12),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 14),
                lexeme: String::from("1_234.5"),
            },
            Token::Dot {
                loc: make_loc!(file_name, 1, 21),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 22),
                lexeme: String::from("add"),
            },
            Token::LParen {
                loc: make_loc!(file_name, 1, 25),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 26),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 28),
                lexeme: String::from("1_234.567_890"),
            },
            Token::Dot {
                loc: make_loc!(file_name, 1, 41),
            },
            Token::Ident {
                loc: make_loc!(file_name, 1, 42),
                lexeme: String::from("big"),
            },
            Token::LParen {
                loc: make_loc!(file_name, 1, 45),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 46),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 47),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_grouping_tokens() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("()[]{}"));
        let expected = vec![
            Token::LParen {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::RParen {
                loc: make_loc!(file_name, 1, 2),
            },
            Token::LBracket {
                loc: make_loc!(file_name, 1, 3),
            },
            Token::RBracket {
                loc: make_loc!(file_name, 1, 4),
            },
            Token::LBrace {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::RBrace {
                loc: make_loc!(file_name, 1, 6),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 7),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_boolean_tokens() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("|&!>< |= &= != == >= <="));
        let expected = vec![
            Token::Pipe {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::Ampersand {
                loc: make_loc!(file_name, 1, 2),
            },
            Token::Bang {
                loc: make_loc!(file_name, 1, 3),
            },
            Token::Greater {
                loc: make_loc!(file_name, 1, 4),
            },
            Token::Less {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::PipeEqual {
                loc: make_loc!(file_name, 1, 7),
            },
            Token::AmpersandEqual {
                loc: make_loc!(file_name, 1, 10),
            },
            Token::BangEqual {
                loc: make_loc!(file_name, 1, 13),
            },
            Token::EqualEqual {
                loc: make_loc!(file_name, 1, 16),
            },
            Token::GreaterEqual {
                loc: make_loc!(file_name, 1, 19),
            },
            Token::LessEqual {
                loc: make_loc!(file_name, 1, 22),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 24),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_arithmetic_tokens() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("+-*/% += -= *= /= %= ="));
        let expected = vec![
            Token::Plus {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::Minus {
                loc: make_loc!(file_name, 1, 2),
            },
            Token::Star {
                loc: make_loc!(file_name, 1, 3),
            },
            Token::Slash {
                loc: make_loc!(file_name, 1, 4),
            },
            Token::Percent {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::PlusEqual {
                loc: make_loc!(file_name, 1, 7),
            },
            Token::MinusEqual {
                loc: make_loc!(file_name, 1, 10),
            },
            Token::StarEqual {
                loc: make_loc!(file_name, 1, 13),
            },
            Token::SlashEqual {
                loc: make_loc!(file_name, 1, 16),
            },
            Token::PercentEqual {
                loc: make_loc!(file_name, 1, 19),
            },
            Token::Equal {
                loc: make_loc!(file_name, 1, 22),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 23),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_scope_tokens() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("@global @file @named"));
        let expected = vec![
            Token::GlobalScope {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::FileScope {
                loc: make_loc!(file_name, 1, 9),
            },
            Token::NamedScope {
                loc: make_loc!(file_name, 1, 15),
                lexeme: String::from("named"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 21),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_separators_tokens() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from(":,"));
        let expected = vec![
            Token::Colon {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::Comma {
                loc: make_loc!(file_name, 1, 2),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 3),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_range_tokens() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("... ..<"));
        let expected = vec![
            Token::RangeInc {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::RangeEx {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 8),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_string_literal() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("\"Some String\""));
        let expected = vec![
            Token::StringLiteral {
                loc: make_loc!(file_name, 1, 1),
                lexeme: String::from("Some String"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 1, 14),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        println!("{:?}", tokens);
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_multi_line_string_literal() {
        let file_name = "TestFile";
        let stream = TestStream::new(
            file_name,
            String::from(
                "\"Some
String\"",
            ),
        );
        let expected = vec![
            Token::StringLiteral {
                loc: make_loc!(file_name, 1, 1),
                lexeme: String::from("Some\nString"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 2, 8),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        println!("{:?}", tokens);
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_single_line_comment() {
        println!("Test Single Line Comment");
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("// This is a comment\n"));
        let expected = vec![Token::EOL {
            loc: make_loc!(file_name, 2, 1),
        }];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        println!("{:?}", tokens);
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_mixed_single_line_comment() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("for in 12 // This is a comment\n"));
        let expected = vec![
            Token::For {
                loc: make_loc!(file_name, 1, 1),
            },
            Token::In {
                loc: make_loc!(file_name, 1, 5),
            },
            Token::Number {
                loc: make_loc!(file_name, 1, 8),
                lexeme: String::from("12"),
            },
            Token::EOL {
                loc: make_loc!(file_name, 2, 1),
            },
        ];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        println!("{:?}", tokens);
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_multi_line_comment() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("/* This is a comment */"));
        let expected = vec![Token::EOL {
            loc: make_loc!(file_name, 1, 24),
        }];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        println!("{:?}", tokens);
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_multi_line_across_multiple_lines_comment() {
        let file_name = "TestFile";
        let stream = TestStream::new(
            file_name,
            String::from(
                "/*
                This is a comment
                */",
            ),
        );
        let expected = vec![Token::EOL {
            loc: make_loc!(file_name, 3, 19),
        }];

        let result = scan(stream);
        assert!(result.is_ok());

        let tokens = result.unwrap();
        println!("{:?}", tokens);
        assert_tokens(&tokens, &expected);
    }

    #[test]
    fn test_unknown_token_errors() {
        let file_name = "TestFile";
        let stream = TestStream::new(file_name, String::from("? is unknown"));
        let loc = make_loc!(file_name, 1, 1);
        let expected = vec![anyhow!(
            "{} [{}:{}] {}",
            loc.file,
            loc.line,
            loc.col,
            "Unknown token: ?"
        )];

        if let Err(errors) = scan(stream) {
            assert_errors(&errors, &expected);
        } else {
            panic!("Expects unknown token errors");
        }
    }
}
