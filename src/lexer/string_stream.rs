use crate::lexer::{DoubleLookAhead, Location};

pub struct StringStream {
    data: String,
    index: usize,
    file: String,
    line: i32,
    col: i32,
}

impl StringStream {
    pub fn new(code: String) -> Self {
        Self {
            data: code,
            index: 0,
            file: String::new(),
            line: 1,
            col: 1,
        }
    }

    fn get_nth(&self, index: usize) -> Option<char> {
        if index < self.data.len() {
            Some(self.data.chars().nth(index).unwrap())
        } else {
            None
        }
    }
}

impl DoubleLookAhead for StringStream {
    fn curr(&self) -> char {
        if self.index < self.data.len() {
            self.data.chars().nth(self.index).unwrap()
        } else {
            '\0'
        }
    }

    fn next(&self) -> Option<char> {
        self.get_nth(self.index + 1)
    }

    fn after_next(&self) -> Option<char> {
        self.get_nth(self.index + 2)
    }

    fn advance(&mut self) {
        if self.curr() == '\n' {
            self.line += 1;
            self.col = 0;
        }

        self.index += 1;
        self.col += 1;

        // Remove Windows line-feed characters
        if self.curr() == '\r' {
            self.index += 1;
        }
    }

    fn loc(&self) -> Location {
        Location {
            file: self.file.to_owned(),
            line: self.line,
            col: self.col,
        }
    }
}
