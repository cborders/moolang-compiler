mod check_passes;
mod errors;
mod interpreter;
mod lexer;
mod parser;
mod program;
mod types;

use {crate::program::Program, argh::FromArgs};

#[derive(Debug, FromArgs)]
/// Moo Compiler Arguments
struct CompilerArgs {
    /// files to be compiled
    #[argh(positional)]
    files: Vec<String>,
}

fn main() {
    let args: CompilerArgs = argh::from_env();
    if args.files.len() > 0 {
        run_files(args);
    } else {
        run_prompt();
    }
}

fn run_files(args: CompilerArgs) {
    let mut program = Program::new();

    if let Err(errors) = lexer::lex(args.files, &mut program) {
        for error in errors {
            eprintln!("{:?}", error);
        }
        return;
    }

    if let Err(errors) = parser::parse(&mut program) {
        for error in errors {
            eprintln!("{:?}", error);
        }
        return;
    }

    if let Err(errors) = check_passes::check(&mut program) {
        for error in errors {
            eprintln!("{:?}", error);
        }
        return;
    }

    if let Err(error) = interpreter::execute(&mut program) {
        eprintln!("{:?}", error);
    }
}

fn run_prompt() {
    // const PROMPT: &str = if cfg!(windows) {
    //     "moo > "
    // } else {
    //     "\u{1F42E} > "
    // };
    // let mut stdout = std::io::stdout();
    // let stdin = std::io::stdin();

    // let global_env = Rc::new(Environment::new());
    // let file_env = Rc::new(global_env.push_new());
    // let mut repl_file = File::new(String::from("REPL"), Rc::clone(&file_env));

    // loop {
    //     stdout.write(PROMPT.as_ref()).unwrap();
    //     stdout.flush().expect("IO Error");

    //     let mut line = String::new();
    //     let _bytes = stdin.read_line(&mut line).unwrap();

    //     if "quit" == line.trim() {
    //         break;
    //     }

    //     match scan(StringStream::new(line)) {
    //         Ok(mut tokens) => {
    //             repl_file.tokens.append(&mut tokens);
    //         }
    //         Err(errors) => {
    //             for error in errors {
    //                 eprintln!("{:?}", error);
    //             }
    //             continue;
    //         }
    //     }

    //     repl_file.env.clear();

    //     if let Err(errors) = parse(&mut repl_file) {
    //         for error in errors {
    //             eprintln!("{:?}", error);
    //         }
    //     }

    //     let mut named_scopes: HashMap<String, Rc<Environment>> = HashMap::new();
    //     if let Err(error) = interpreter::execute(&repl_file, &mut named_scopes) {
    //         eprintln!("{:?}", error);
    //     }
    // }
}
