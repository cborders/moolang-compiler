use {
    crate::lexer::{Location, Token},
    std::fmt,
};

#[derive(Clone)]
pub enum Expr {
    Assignment {
        ident: Token,
        expr: Box<Expr>,
    },
    Binary {
        left: Box<Expr>,
        oper: Token,
        right: Box<Expr>,
    },
    BoolLiteral {
        value: Token,
    },
    Cast {
        expr: Box<Expr>,
        cast_type: Token,
    },
    Grouping {
        expr: Box<Expr>,
    },
    StringLiteral {
        value: Token,
    },
    NumberLiteral {
        value: Token,
    },
    Unary {
        oper: Token,
        expr: Box<Expr>,
    },
    Var {
        ident: Token,
    },
}

impl Expr {
    pub fn token(&self) -> &Token {
        match self {
            Expr::Assignment { ident, .. } => ident,
            Expr::Binary { oper, .. } => oper,
            Expr::BoolLiteral { value } => value,
            Expr::Cast { cast_type, .. } => cast_type,
            Expr::Grouping { expr } => expr.token(),
            Expr::StringLiteral { value } => value,
            Expr::NumberLiteral { value } => value,
            Expr::Unary { oper, .. } => oper,
            Expr::Var { ident } => ident,
        }
    }

    pub fn location(&self) -> &Location {
        self.token().location()
    }
}

impl fmt::Debug for Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            Expr::Assignment { ident, expr } => write!(f, "({} = {:?})", ident, expr),
            Expr::Binary { left, oper, right } => write!(f, "({:?} {} {:?})", left, oper, right),
            Expr::BoolLiteral { value } => write!(f, "{}", value),
            Expr::Cast { expr, cast_type } => write!(f, "({:?} as {})", expr, cast_type),
            Expr::Grouping { expr } => write!(f, "(group {:?})", expr),
            Expr::StringLiteral { value } => write!(f, "{}", value),
            Expr::NumberLiteral { value } => write!(f, "{}", value),
            Expr::Unary { oper, expr } => write!(f, "({} {:?})", oper, expr),
            Expr::Var { ident } => write!(f, "({})", ident),
        }
    }
}
