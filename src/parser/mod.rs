mod exprs;
mod stmts;
mod token_stream;

pub use exprs::Expr;
pub use stmts::{ConditionalStatement, Stmt};

use {
    crate::{errors::format_error, lexer::Token, program::Program},
    anyhow::Error,
    if_chain::if_chain,
    token_stream::TokenStream,
};

macro_rules! try_eat {
    ($token:pat, $stream:ident, $message:expr) => {
        if let $token = $stream.curr() {
            $stream.advance();
        } else {
            return Err(vec![format_error($stream.curr(), $message)]);
        }
    };
}

pub fn parse(program: &mut Program) -> Result<(), Vec<Error>> {
    let mut errors = vec![];
    for mut file in &mut program.files {
        match parse_tokens(&file.tokens) {
            Ok(stmts) => file.stmts = stmts,
            Err(mut parsed_errors) => errors.append(&mut parsed_errors),
        }
    }

    if errors.len() > 0 {
        Err(errors)
    } else {
        Ok(())
    }
}

fn parse_tokens(tokens: &Vec<Token>) -> Result<Vec<Stmt>, Vec<Error>> {
    let mut stmts: Vec<Stmt> = vec![];
    let mut errors: Vec<Error> = vec![];
    let mut stream = TokenStream::new(tokens);

    while let Some(_) = stream.next() {
        eat_blank_lines(&mut stream);

        // Make sure we haven't hit the end
        if let None = stream.next() {
            break;
        }

        match declaration(&mut stream) {
            Ok(stmt) => stmts.push(stmt),
            Err(mut error) => errors.append(&mut error),
        }
    }

    if errors.len() > 0 {
        Err(errors)
    } else {
        Ok(stmts)
    }
}

fn eat_blank_lines(stream: &mut TokenStream) {
    loop {
        if_chain! {
            if let Token::EOL { loc: _ } = stream.curr();
            if let Some(_) = stream.next();
            then {
                stream.advance();
            } else {
                break;
            }
        }
    }
}

fn declaration(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    match stream.curr() {
        Token::Var { loc: _ } => var_declaration(stream, false),
        Token::Let { loc: _ } => var_declaration(stream, true),
        _ => statement(stream),
    }
}

fn var_declaration(stream: &mut TokenStream, constant: bool) -> Result<Stmt, Vec<Error>> {
    // Eat decl token
    stream.advance();

    // Expect identifier
    if let Token::Ident { loc: _, lexeme: _ } = stream.curr() {
        let ident = stream.curr().clone();
        stream.advance();

        // Check for type
        let var_type = if let Token::Colon { loc: _ } = stream.curr() {
            // Eat colon
            stream.advance();
            if stream.curr().is_type() {
                let var_type = stream.curr().clone();
                stream.advance();
                Some(var_type)
            } else {
                return Err(vec![format_error(
                    stream.curr(),
                    "Expected a type to follow the ':' is a declaration",
                )]);
            }
        } else {
            None
        };

        // Check for an initializer
        let expr = if let Token::Equal { loc: _ } = stream.curr() {
            // Eat equal sign
            stream.advance();
            let expr = expression(stream).or_else(|error| return Err(vec![error]))?;
            Some(Box::new(expr))
        } else {
            None
        };

        Ok(Stmt::VarDecl {
            ident,
            constant,
            var_type,
            expr,
        })
    } else {
        Err(vec![format_error(
            stream.curr(),
            &format!(
                "Expected an identifier after {}",
                if constant { "let" } else { "var" }
            ),
        )])
    }
}

fn statement(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    match stream.curr() {
        Token::GlobalScope { loc: _ } => scope_statement(stream),
        Token::FileScope { loc: _ } => scope_statement(stream),
        Token::NamedScope { loc: _, lexeme: _ } => scope_statement(stream),
        Token::Print { loc: _ } => print_statement(stream),
        Token::When { loc: _ } => when_statement(stream),
        Token::While { loc: _ } => while_statement(stream),
        Token::LBrace { loc: _ } => block(stream),
        _ => expression_statement(stream),
    }
}

fn scope_statement(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    let token = stream.curr().to_owned();
    stream.advance();
    Ok(Stmt::Scope { token })
}

fn print_statement(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    try_eat!(Token::Print { loc: _ }, stream, "Expected 'print'");

    let expr = expression(stream).or_else(|error| return Err(vec![error]))?;

    Ok(Stmt::Print {
        expr: Box::new(expr),
    })
}

fn when_statement(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    try_eat!(Token::When { loc: _ }, stream, "Expected 'when'");

    // Allow new lines here
    eat_blank_lines(stream);

    match stream.curr() {
        Token::LBrace { loc: _ } => when_full(stream),
        Token::LParen { loc: _ } => when_full(stream),
        _ => when_simple(stream),
    }
}

fn when_simple(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    let condition = match expression(stream) {
        Ok(expr) => Box::new(expr),
        Err(error) => return Err(vec![error]),
    };

    // Allow new lines here
    eat_blank_lines(stream);

    let statement = Box::new(block(stream)?);

    // Allow new lines here
    eat_blank_lines(stream);

    let else_stmt = if let Token::Else { loc: _ } = stream.curr() {
        // Eat "else" keyword
        stream.advance();

        // Allow new lines here
        eat_blank_lines(stream);

        Some(Box::new(block(stream)?))
    } else {
        None
    };

    Ok(Stmt::When {
        conditions: vec![ConditionalStatement {
            condition,
            statement,
        }],
        else_stmt,
    })
}

fn when_full(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    // Is there a capture?
    let capture = if let Token::LParen { loc: _ } = stream.curr() {
        // Eat opening paren
        stream.advance();

        let expr = match expression(stream) {
            Ok(expr) => expr,
            Err(error) => return Err(vec![error]),
        };

        try_eat!(
            Token::RParen { loc: _ },
            stream,
            "Expected ')' after closure"
        );
        Some(expr)
    } else {
        None
    };

    // Allow new lines here
    eat_blank_lines(stream);

    // TODO(Casey): Come up with a better message here.
    try_eat!(
        Token::LBrace { loc: _ },
        stream,
        "Expected '{' after closure"
    );

    let mut conditions = vec![];
    loop {
        // Watch for the end of the regular conditional cases
        match stream.curr() {
            Token::RBrace { loc: _ } => break,
            Token::Else { loc: _ } => break,
            _ => (),
        }

        // Allow new lines here
        eat_blank_lines(stream);

        let condition = match expression(stream) {
            Ok(expr) => expr,
            Err(error) => return Err(vec![error]),
        };

        try_eat!(
            Token::Colon { loc: _ },
            stream,
            "Expected ':' after condition"
        );

        // Allow new lines here
        eat_blank_lines(stream);

        let statement = Box::new(statement(stream)?);

        let condition = if let Some(ref capture) = capture {
            let left = capture.clone();
            Box::new(Expr::Binary {
                left: Box::new(left),
                oper: Token::EqualEqual {
                    loc: condition.location().clone(),
                },
                right: Box::new(condition),
            })
        } else {
            Box::new(condition)
        };

        conditions.push(ConditionalStatement {
            condition,
            statement,
        });

        // Allow new lines here
        eat_blank_lines(stream);
    }

    let else_stmt = if let Token::Else { loc: _ } = stream.curr() {
        // Eat "else" keyword
        stream.advance();

        try_eat!(
            Token::Colon { loc: _ },
            stream,
            "Expected ':' after condition"
        );

        // Allow new lines here
        eat_blank_lines(stream);

        Some(Box::new(statement(stream)?))
    } else {
        None
    };

    // Allow new lines here
    eat_blank_lines(stream);

    // Eat closing brace
    stream.advance();

    Ok(Stmt::When {
        conditions,
        else_stmt,
    })
}

fn while_statement(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    try_eat!(Token::While { loc: _ }, stream, "Expected 'while'");

    let condition = match expression(stream) {
        Ok(expr) => Box::new(expr),
        Err(error) => return Err(vec![error]),
    };

    let block = Box::new(block(stream)?);

    Ok(Stmt::While { condition, block })
}

fn block(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    try_eat!(
        Token::LBrace { loc: _ },
        stream,
        "Expected block to start with '{'"
    );

    let mut stmts = vec![];
    let mut errors = vec![];

    while let Some(_) = stream.next() {
        // Allow new lines here
        eat_blank_lines(stream);

        if let Token::RBrace { loc: _ } = stream.curr() {
            // Eat closing brace
            stream.advance();
            break;
        } else {
            // Make sure we haven't hit the end
            if let None = stream.next() {
                break;
            }

            match declaration(stream) {
                Ok(stmt) => stmts.push(Box::new(stmt)),
                Err(mut error) => errors.append(&mut error),
            }
        }
    }

    if errors.len() > 0 {
        Err(errors)
    } else {
        Ok(Stmt::Block { stmts })
    }
}

fn expression_statement(stream: &mut TokenStream) -> Result<Stmt, Vec<Error>> {
    let expr = expression(stream).or_else(|error| return Err(vec![error]))?;

    Ok(Stmt::Expr {
        expr: Box::new(expr),
    })
}

fn expression(stream: &mut TokenStream) -> Result<Expr, Error> {
    let expr = assignment(stream)?;
    Ok(expr)
}

fn assignment(stream: &mut TokenStream) -> Result<Expr, Error> {
    if_chain! {
        if let Token::Ident { loc: _, lexeme: _ } = stream.curr();
        if let Some(Token::Equal { loc: _ }) = stream.next();
        then {
            let ident = stream.curr().clone();
            // Pop off the identifier and the equal token
            stream.advance();
            stream.advance();
            let expr = Box::new(expression(stream)?);
            Ok(Expr::Assignment { ident, expr })
        } else {
            logic_or(stream)
        }
    }
}

fn logic_or(stream: &mut TokenStream) -> Result<Expr, Error> {
    let mut expr = logic_and(stream)?;

    while match stream.curr() {
        Token::Pipe { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = logic_and(stream)?;
        expr = Expr::Binary {
            left: Box::new(expr),
            oper,
            right: Box::new(right),
        };
    }

    Ok(expr)
}

fn logic_and(stream: &mut TokenStream) -> Result<Expr, Error> {
    let mut expr = equality(stream)?;

    while match stream.curr() {
        Token::Ampersand { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = equality(stream)?;
        expr = Expr::Binary {
            left: Box::new(expr),
            oper,
            right: Box::new(right),
        };
    }

    Ok(expr)
}

fn equality(stream: &mut TokenStream) -> Result<Expr, Error> {
    let mut expr = comparison(stream)?;

    while match stream.curr() {
        Token::BangEqual { loc: _ } => true,
        Token::EqualEqual { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = comparison(stream)?;
        expr = Expr::Binary {
            left: Box::new(expr),
            oper,
            right: Box::new(right),
        };
    }

    Ok(expr)
}

fn comparison(stream: &mut TokenStream) -> Result<Expr, Error> {
    let mut expr = term(stream)?;

    while match stream.curr() {
        Token::Greater { loc: _ } => true,
        Token::GreaterEqual { loc: _ } => true,
        Token::Less { loc: _ } => true,
        Token::LessEqual { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = term(stream)?;
        expr = Expr::Binary {
            left: Box::new(expr),
            oper,
            right: Box::new(right),
        };
    }

    Ok(expr)
}

fn term(stream: &mut TokenStream) -> Result<Expr, Error> {
    let mut expr = factor(stream)?;

    while match stream.curr() {
        Token::Minus { loc: _ } => true,
        Token::Plus { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = factor(stream)?;
        expr = Expr::Binary {
            left: Box::new(expr),
            oper,
            right: Box::new(right),
        };
    }

    Ok(expr)
}

fn factor(stream: &mut TokenStream) -> Result<Expr, Error> {
    let mut expr = unary(stream)?;

    while match stream.curr() {
        Token::Slash { loc: _ } => true,
        Token::Star { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = unary(stream)?;
        expr = Expr::Binary {
            left: Box::new(expr),
            oper,
            right: Box::new(right),
        };
    }

    Ok(expr)
}

fn unary(stream: &mut TokenStream) -> Result<Expr, Error> {
    if match stream.curr() {
        Token::Bang { loc: _ } => true,
        Token::Minus { loc: _ } => true,
        _ => false,
    } {
        let oper = stream.curr().clone();
        stream.advance();
        let right = unary(stream)?;
        Ok(Expr::Unary {
            oper,
            expr: Box::new(right),
        })
    } else {
        cast(stream)
    }
}

fn cast(stream: &mut TokenStream) -> Result<Expr, Error> {
    let expr = primary(stream)?;
    if let Token::As { loc: _ } = stream.curr() {
        // Pop 'as' and get type
        stream.advance();

        if stream.curr().is_type() {
            let cast_type = stream.curr().clone();
            stream.advance();
            Ok(Expr::Cast {
                expr: Box::new(expr),
                cast_type,
            })
        } else {
            Err(format_error(
                stream.curr(),
                &format!("Expected a type after 'as' but found {}", stream.curr()),
            ))
        }
    } else {
        Ok(expr)
    }
}

fn primary(stream: &mut TokenStream) -> Result<Expr, Error> {
    let result = match stream.curr() {
        Token::True { loc: _ } => Ok(Expr::BoolLiteral {
            value: stream.curr().clone(),
        }),
        Token::False { loc: _ } => Ok(Expr::BoolLiteral {
            value: stream.curr().clone(),
        }),
        Token::Number { loc: _, lexeme: _ } => Ok(Expr::NumberLiteral {
            value: stream.curr().clone(),
        }),
        Token::StringLiteral { loc: _, lexeme: _ } => Ok(Expr::StringLiteral {
            value: stream.curr().clone(),
        }),
        Token::Ident { loc: _, lexeme: _ } => Ok(Expr::Var {
            ident: stream.curr().clone(),
        }),
        Token::LParen { loc: _ } => {
            // Eat the starting paren
            stream.advance();
            let expr = expression(stream)?;
            // Expect a trailing paren
            if let Token::RParen { loc: _ } = stream.curr() {
                Ok(Expr::Grouping {
                    expr: Box::new(expr),
                })
            } else {
                Err(format_error(
                    stream.curr(),
                    &format!("Expected closing paren but found {}", stream.curr()),
                ))
            }
        }
        _ => Err(format_error(
            stream.curr(),
            &format!("Unrecognized token: {}", stream.curr()),
        )),
    };

    stream.advance();
    result
}
