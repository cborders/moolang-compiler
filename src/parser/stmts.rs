use {super::exprs::Expr, crate::lexer::Token, std::fmt};

#[derive(Debug)]
pub struct ConditionalStatement {
    pub condition: Box<Expr>,
    pub statement: Box<Stmt>,
}

pub enum Stmt {
    Block {
        stmts: Vec<Box<Stmt>>,
    },
    Expr {
        expr: Box<Expr>,
    },
    Print {
        expr: Box<Expr>,
    },
    Scope {
        token: Token,
    },
    VarDecl {
        ident: Token,
        constant: bool,
        var_type: Option<Token>,
        expr: Option<Box<Expr>>,
    },
    When {
        conditions: Vec<ConditionalStatement>,
        else_stmt: Option<Box<Stmt>>,
    },
    While {
        condition: Box<Expr>,
        block: Box<Stmt>,
    },
}

impl fmt::Debug for Stmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            Stmt::Block { stmts } => {
                write!(f, "{{\n")?;
                for stmt in stmts {
                    write!(f, "{:?}\n", stmt)?;
                }
                write!(f, "}}\n")
            }
            Stmt::Expr { expr } => write!(f, "{:?}\n", expr),
            Stmt::Print { expr } => write!(f, "{:?}\n", expr),
            Stmt::Scope { token } => write!(f, "@{}\n", token),
            Stmt::VarDecl {
                ident,
                constant,
                var_type,
                expr,
            } => {
                let constness = if *constant { "constant" } else { "mutable" };

                let var_type = if let Some(token) = var_type {
                    format!(" : {:?}", token)
                } else {
                    String::new()
                };

                let expr = if let Some(expr) = expr {
                    format!(" = {:?}", expr)
                } else {
                    String::new()
                };

                write!(f, "{} {}{}{}", constness, ident, var_type, expr)
            }
            Stmt::When {
                conditions,
                else_stmt,
            } => {
                if conditions.len() == 1 {
                    let else_stmt = if let Some(stmt) = else_stmt {
                        format!(" else {:?}", stmt)
                    } else {
                        String::new()
                    };

                    let condition = &conditions[1];
                    write!(
                        f,
                        "when {:?} : {:?}{}",
                        condition.condition, condition.statement, else_stmt
                    )
                } else {
                    write!(f, "when {{")?;
                    for condition in conditions {
                        write!(
                            f,
                            "    {:?} : {:?}\n",
                            condition.condition, condition.statement
                        )?;
                    }
                    if let Some(stmt) = else_stmt {
                        write!(f, "    else : {:?}\n", stmt)?;
                    }
                    write!(f, "}}")
                }
            }
            Stmt::While { condition, block } => {
                write!(f, "while {:?}{:?}", condition, block)
            }
        }
    }
}
