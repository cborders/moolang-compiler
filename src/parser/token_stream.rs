use crate::lexer::Token;

pub struct TokenStream<'a> {
    tokens: &'a Vec<Token>,
    index: usize,
}

impl<'a> TokenStream<'a> {
    pub fn new(tokens: &'a Vec<Token>) -> Self {
        Self { tokens, index: 0 }
    }

    pub fn curr(&self) -> &Token {
        &self.tokens[self.index]
    }

    pub fn next(&self) -> Option<&Token> {
        if self.index + 1 < self.tokens.len() {
            Some(&self.tokens[self.index + 1])
        } else {
            None
        }
    }

    pub fn advance(&mut self) {
        if let Some(_) = self.next() {
            self.index += 1;
        }
    }
}
