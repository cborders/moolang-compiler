use crate::{lexer::Token, parser::Stmt};

pub struct Program {
    pub(crate) files: Vec<File>,
}

impl Program {
    pub fn new() -> Self {
        Self { files: vec![] }
    }
}

pub struct File {
    pub path: String,
    pub tokens: Vec<Token>,
    pub stmts: Vec<Stmt>,
}

impl File {
    pub fn new(path: String) -> Self {
        File {
            path,
            tokens: vec![],
            stmts: vec![],
        }
    }
}
