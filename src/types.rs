use {
    crate::{errors::format_error, lexer::Token},
    anyhow::{anyhow, Error},
    std::{any::TypeId, fmt},
};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum MooType {
    Int8,
    Int16,
    Int32,
    Int64,
    Uint8,
    Uint16,
    Uint32,
    Uint64,
    Float32,
    Float64,
    String,
    Bool,
}

impl MooType {
    // TODO(Casey): When https://github.com/rust-lang/rust/issues/77125 is
    // resolved replace this with a match.
    pub fn from_rust(type_id: TypeId) -> Result<MooType, Error> {
        if type_id == TypeId::of::<i8>() {
            Ok(MooType::Int8)
        } else if type_id == TypeId::of::<i16>() {
            Ok(MooType::Int16)
        } else if type_id == TypeId::of::<i32>() {
            Ok(MooType::Int32)
        } else if type_id == TypeId::of::<i64>() {
            Ok(MooType::Int64)
        } else if type_id == TypeId::of::<u8>() {
            Ok(MooType::Uint8)
        } else if type_id == TypeId::of::<u16>() {
            Ok(MooType::Uint16)
        } else if type_id == TypeId::of::<u32>() {
            Ok(MooType::Uint32)
        } else if type_id == TypeId::of::<u64>() {
            Ok(MooType::Uint64)
        } else if type_id == TypeId::of::<f32>() {
            Ok(MooType::Float32)
        } else if type_id == TypeId::of::<f64>() {
            Ok(MooType::Float64)
        } else if type_id == TypeId::of::<String>() {
            Ok(MooType::String)
        } else if type_id == TypeId::of::<bool>() {
            Ok(MooType::Bool)
        } else {
            Err(anyhow!("Unrecognized type: {:?}", type_id))
        }
    }

    pub fn to_rust(moo_type: MooType) -> TypeId {
        match moo_type {
            MooType::Int8 => TypeId::of::<i8>(),
            MooType::Int16 => TypeId::of::<i16>(),
            MooType::Int32 => TypeId::of::<i32>(),
            MooType::Int64 => TypeId::of::<i64>(),
            MooType::Uint8 => TypeId::of::<u8>(),
            MooType::Uint16 => TypeId::of::<u16>(),
            MooType::Uint32 => TypeId::of::<u32>(),
            MooType::Uint64 => TypeId::of::<u64>(),
            MooType::Float32 => TypeId::of::<f32>(),
            MooType::Float64 => TypeId::of::<f64>(),
            MooType::String => TypeId::of::<String>(),
            MooType::Bool => TypeId::of::<bool>(),
        }
    }

    pub fn from_token(token: &Token) -> Result<MooType, Error> {
        match token {
            Token::Int8 { loc: _ } => Ok(MooType::Int8),
            Token::Int16 { loc: _ } => Ok(MooType::Int16),
            Token::Int32 { loc: _ } => Ok(MooType::Int32),
            Token::Int64 { loc: _ } => Ok(MooType::Int64),
            Token::Uint8 { loc: _ } => Ok(MooType::Uint8),
            Token::Uint16 { loc: _ } => Ok(MooType::Uint16),
            Token::Uint32 { loc: _ } => Ok(MooType::Uint32),
            Token::Uint64 { loc: _ } => Ok(MooType::Uint64),
            Token::Float32 { loc: _ } => Ok(MooType::Float32),
            Token::Float64 { loc: _ } => Ok(MooType::Float64),
            Token::StringType { loc: _ } => Ok(MooType::String),
            _ => Err(format_error(token, &format!("Unknown type: {}", token))),
        }
    }

    pub fn result_type(left: &MooType, right: &MooType) -> Result<MooType, Error> {
        match left {
            MooType::Int8 => {
                match right {
                    MooType::Int8 => Ok(MooType::Int8),
                    MooType::Int16 => Ok(MooType::Int16),
                    MooType::Int32 => Ok(MooType::Int32),
                    MooType::Int64 => Ok(MooType::Int64),
                    MooType::Uint8 | MooType::Uint16 | MooType::Uint32 | MooType::Uint64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Int16 => {
                match right {
                    MooType::Int8 => Ok(MooType::Int16),
                    MooType::Int16 => Ok(MooType::Int16),
                    MooType::Int32 => Ok(MooType::Int32),
                    MooType::Int64 => Ok(MooType::Int64),
                    MooType::Uint8 | MooType::Uint16 | MooType::Uint32 | MooType::Uint64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Int32 => {
                match right {
                    MooType::Int8 => Ok(MooType::Int32),
                    MooType::Int16 => Ok(MooType::Int32),
                    MooType::Int32 => Ok(MooType::Int32),
                    MooType::Int64 => Ok(MooType::Int64),
                    MooType::Uint8 | MooType::Uint16 | MooType::Uint32 | MooType::Uint64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Int64 => {
                match right {
                    MooType::Int8 => Ok(MooType::Int64),
                    MooType::Int16 => Ok(MooType::Int64),
                    MooType::Int32 => Ok(MooType::Int64),
                    MooType::Int64 => Ok(MooType::Int64),
                    MooType::Uint8 | MooType::Uint16 | MooType::Uint32 | MooType::Uint64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Uint8 => {
                match right {
                    MooType::Int8 | MooType::Int16 | MooType::Int32 | MooType::Int64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Uint8 => Ok(MooType::Uint8),
                    MooType::Uint16 => Ok(MooType::Uint16),
                    MooType::Uint32 => Ok(MooType::Uint32),
                    MooType::Uint64 => Ok(MooType::Uint64),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Uint16 => {
                match right {
                    MooType::Int8 | MooType::Int16 | MooType::Int32 | MooType::Int64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Uint8 => Ok(MooType::Uint16),
                    MooType::Uint16 => Ok(MooType::Uint16),
                    MooType::Uint32 => Ok(MooType::Uint32),
                    MooType::Uint64 => Ok(MooType::Uint64),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Uint32 => {
                match right {
                    MooType::Int8 | MooType::Int16 | MooType::Int32 | MooType::Int64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Uint8 => Ok(MooType::Uint32),
                    MooType::Uint16 => Ok(MooType::Uint32),
                    MooType::Uint32 => Ok(MooType::Uint32),
                    MooType::Uint64 => Ok(MooType::Uint64),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Uint64 => {
                match right {
                    MooType::Int8 | MooType::Int16 | MooType::Int32 | MooType::Int64 => Err(anyhow!("Cannot mix signed and unsigned types left: {:?} and right: {:?}", left, right)),
                    MooType::Uint8 => Ok(MooType::Uint64),
                    MooType::Uint16 => Ok(MooType::Uint64),
                    MooType::Uint32 => Ok(MooType::Uint64),
                    MooType::Uint64 => Ok(MooType::Uint64),
                    MooType::Float32 | MooType::Float64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Float32 => {
                match right {
                    MooType::Int8 | MooType::Int16 | MooType::Int32 | MooType::Int64 | MooType::Uint8 | MooType::Uint16 | MooType::Uint32 | MooType::Uint64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    MooType::Float32 => Ok(MooType::Float32),
                    MooType::Float64 => Ok(MooType::Float64),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Float64 => {
                match right {
                    MooType::Int8 | MooType::Int16 | MooType::Int32 | MooType::Int64 | MooType::Uint8 | MooType::Uint16 | MooType::Uint32 | MooType::Uint64 => Err(anyhow!("Cannot mix floating point and non-floating point types left: {:?} and right: {:?}", left, right)),
                    MooType::Float32 => Ok(MooType::Float64),
                    MooType::Float64 => Ok(MooType::Float64),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::String => {
                match right {
                    MooType::String => Ok(MooType::String),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
            MooType::Bool => {
                match right {
                    MooType::Bool => Ok(MooType::Bool),
                    _ => Err(anyhow!("Operation not valid for left: {:?} and right: {:?}", left, right))
                }
            },
        }
    }

    pub fn is_numeric(&self) -> bool {
        match self {
            MooType::Int8 => true,
            MooType::Int16 => true,
            MooType::Int32 => true,
            MooType::Int64 => true,
            MooType::Uint8 => true,
            MooType::Uint16 => true,
            MooType::Uint32 => true,
            MooType::Uint64 => true,
            MooType::Float32 => true,
            MooType::Float64 => true,
            _ => false,
        }
    }
}

impl fmt::Display for MooType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            MooType::Int8 => write!(f, "int8"),
            MooType::Int16 => write!(f, "int16"),
            MooType::Int32 => write!(f, "int32"),
            MooType::Int64 => write!(f, "int64"),
            MooType::Uint8 => write!(f, "uint8"),
            MooType::Uint16 => write!(f, "uint16"),
            MooType::Uint32 => write!(f, "uint32"),
            MooType::Uint64 => write!(f, "uint64"),
            MooType::Float32 => write!(f, "float32"),
            MooType::Float64 => write!(f, "float64"),
            MooType::String => write!(f, "string"),
            MooType::Bool => write!(f, "bool"),
        }
    }
}
