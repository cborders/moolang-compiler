var label : string
let points = 99

when points < 50 { label = "low" }
else { label = "high" }

print label

var grade : string
let score = 75
when
{
    score >= 90 : grade = "A"
    score >= 80 : grade = "B"
    score >= 70 : grade = "C"
    score >= 60 : grade = "D"
    else : grade = "F"
}
print grade



let level = 6
when (level) {
    0 : print "Noob"
    1 : print "Fledgling"
    2 : print "Novice"
    3 : print "Wizard"
    4 : print "Archmage"
    5 : print "Demigod"
    else : print "Dead"
}
